# Three Tasks:

1. Predict the Genre of a song using the lyrics :guitar: :notes: 

2. Predict the Artist using the lyrics and by applying models from 1. :microphone: 

3. Generate a script for the TV series House MD :tv:

![](https://c.tenor.com/CpZC59y-D-QAAAAC/house-md-dr-house.gif "House MD")
