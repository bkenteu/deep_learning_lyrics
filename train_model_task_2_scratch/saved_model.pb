��
�(�'
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
K
Bincount
arr
size
weights"T	
bins"T"
Ttype:
2	
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
�
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

�
Cumsum
x"T
axis"Tidx
out"T"
	exclusivebool( "
reversebool( " 
Ttype:
2	"
Tidxtype0:
2	
R
Equal
x"T
y"T
z
"	
Ttype"$
incompatible_shape_errorbool(�
W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
=
Greater
x"T
y"T
z
"
Ttype:
2	
�
HashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetype�
.
Identity

input"T
output"T"	
Ttype
w
LookupTableFindV2
table_handle
keys"Tin
default_value"Tout
values"Tout"
Tintype"
Touttype�
b
LookupTableImportV2
table_handle
keys"Tin
values"Tout"
Tintype"
Touttype�
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
�
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 ":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C
>
Maximum
x"T
y"T
z"T"
Ttype:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
>
Minimum
x"T
y"T
z"T"
Ttype:
2	
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
�
RaggedTensorToTensor
shape"Tshape
values"T
default_value"T:
row_partition_tensors"Tindex*num_row_partition_tensors
result"T"	
Ttype"
Tindextype:
2	"
Tshapetype:
2	"$
num_row_partition_tensorsint(0"#
row_partition_typeslist(string)
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
�
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	�
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
A
SelectV2
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
9
Softmax
logits"T
softmax"T"
Ttype:
2
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
m
StaticRegexReplace	
input

output"
patternstring"
rewritestring"
replace_globalbool(
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
StringLower	
input

output"
encodingstring 
e
StringSplitV2	
input
sep
indices	

values	
shape	"
maxsplitint���������
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.8.02v2.8.0-0-g3f878cff5b68��
�
embedding/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�Nd*%
shared_nameembedding/embeddings
~
(embedding/embeddings/Read/ReadVariableOpReadVariableOpembedding/embeddings*
_output_shapes
:	�Nd*
dtype0
{
conv1d/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:d�*
shared_nameconv1d/kernel
t
!conv1d/kernel/Read/ReadVariableOpReadVariableOpconv1d/kernel*#
_output_shapes
:d�*
dtype0
o
conv1d/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_nameconv1d/bias
h
conv1d/bias/Read/ReadVariableOpReadVariableOpconv1d/bias*
_output_shapes	
:�*
dtype0
z
dense_8/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*
shared_namedense_8/kernel
s
"dense_8/kernel/Read/ReadVariableOpReadVariableOpdense_8/kernel* 
_output_shapes
:
��*
dtype0
q
dense_8/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_namedense_8/bias
j
 dense_8/bias/Read/ReadVariableOpReadVariableOpdense_8/bias*
_output_shapes	
:�*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
n

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name494895*
value_dtype0	
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/embedding/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�Nd*,
shared_nameAdam/embedding/embeddings/m
�
/Adam/embedding/embeddings/m/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/m*
_output_shapes
:	�Nd*
dtype0
�
Adam/conv1d/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:d�*%
shared_nameAdam/conv1d/kernel/m
�
(Adam/conv1d/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv1d/kernel/m*#
_output_shapes
:d�*
dtype0
}
Adam/conv1d/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*#
shared_nameAdam/conv1d/bias/m
v
&Adam/conv1d/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv1d/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/dense_8/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/dense_8/kernel/m
�
)Adam/dense_8/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/m* 
_output_shapes
:
��*
dtype0

Adam/dense_8/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*$
shared_nameAdam/dense_8/bias/m
x
'Adam/dense_8/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/embedding/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�Nd*,
shared_nameAdam/embedding/embeddings/v
�
/Adam/embedding/embeddings/v/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/v*
_output_shapes
:	�Nd*
dtype0
�
Adam/conv1d/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:d�*%
shared_nameAdam/conv1d/kernel/v
�
(Adam/conv1d/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv1d/kernel/v*#
_output_shapes
:d�*
dtype0
}
Adam/conv1d/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*#
shared_nameAdam/conv1d/bias/v
v
&Adam/conv1d/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv1d/bias/v*
_output_shapes	
:�*
dtype0
�
Adam/dense_8/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/dense_8/kernel/v
�
)Adam/dense_8/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/v* 
_output_shapes
:
��*
dtype0

Adam/dense_8/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*$
shared_nameAdam/dense_8/bias/v
x
'Adam/dense_8/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/v*
_output_shapes	
:�*
dtype0
G
ConstConst*
_output_shapes
: *
dtype0	*
value	B	 R
H
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 
I
Const_2Const*
_output_shapes
: *
dtype0	*
value	B	 R 
��
Const_3Const*
_output_shapes	
:�N*
dtype0*��
value��B���NBtheByouBiBandBtoBaBmeBmyBitBinBonBthatBofBimByourBallBbeBisBforBloveBdontBweBlikeBsoBknowBbutBitsBwithBupBjustBnoBthisBgotBwhenBwhatBohBdoBgetBcanBnowBifBoutBbabyBgoBdownByoureByeahBoneBseeBtimeBtheyBsheBwasBneverBwantBcantBareBcomeBmakeBnotBsayBletBhaveBcauseBatBwayBfromBbackBtakeBwillBherBillBaintBhowBheBwannaBgonnaBrightBgirlBwereBasBfeelBthereBhereBneedBtellBlifeBawayBbeenBwellBnightBmoreBiveBmanBthatsBcouldBgiveBwhereBheartBdayBtooBbyBaboutBsomeBonlyBworldBgoodBthenBkeepBthinkBorBwhyBlittleBtheresBwhoBaroundBthroughBlookBwontBstillBoverBsaidBthemBagainByaBhadBheyBourBusBlongBeveryBchorusBhisBgottaBeyesBwouldBhomeBoffBmindBputBeverBalwaysBfindBamBbetterBanBnothingBthingsBreallyBgoneBholdBturnBtonightBmuchBhearBboyBniggaBshesBtheseBstopBhimBintoBmoneyBletsBcallBthanB	somethingBthingBnewB
everythingBshitBlaBliveBrunBbeforeBgoingBleaveBshowBdidBtryByoullBevenBheadBstayBhardBemBbelieveBanotherBaloneBfaceBlightByouveBmadeBoohBbadBinsideBrealBpeopleBidBlastBshouldBleftBfallBoldBmineBplayBbigBplaceBpleaseBsameBbreakBnameBwithoutBfuckBhighBwalkBwrongBdanceBbitchBhandBhasBhesBfeelingBcryBdreamBfreeBendBtwoBwhatsBchangeByesBrockBtalkBbodyBsunBlookingBlostBhandsBotherBownBdoneBtheirBniggasBtrueBcareBthoughtBwhileBsweetBenoughBsomeoneBcrazyBsongBgirlsBdieBworkBfireBcomingBgodBbestBsideBmyselfBbringBmaybeBmustBtogetherBnaBrememberBhitBtillByoBwaitingBroundBkissBmoveBskyBforeverB	everybodyBsoulBcomesByoungBfarBwatchBcameBgameBdidntBstartBmightBtoldBrideBwaitBtouchBuhBstandBflyBthoseBdaysBusedBnobodyBbecauseBwishBrainBfoundBrollBblackBboutBmanyBsingBblueBfirstBtimesBmeanBwordsBcoldBpainBdreamsBhopeBknewBtownBhelpBseenBdoorBcloseBahBgettingBloseBonceBgonBfriendBhotBreadyBbeatBlonelyBhellBgoesBalrightBlivingBfriendsBtryingBassBanyBsomebodyBmakesBknowsBdaBtookBmayByallBfightB	sometimesBthoughBafterBverseBpartyBlineBwomanBsureBmusicBcityBwholeBmorningByourselfBtheyreBlieBhappyBuntilBshakeBtodayBelseBopenBsmileBforgetBheardBdeadBmatterBtearsBanythingBdarkB
understandBhurtBgoodbyeBunderBsleepBrunningBlistenBnextBprettyBeasyBlightsBaskBbehindBdeepBboysBtopBroadBhateB	beautifulBbabeBmissBlateBfloorBfineBkindBlordBfeelsBguessBsoundBstreetBairBroomBsawBtruthBeachBwentBtilBmamaBhouseBaliveBarmsBfallingBdamnBgoinBcouldntBwhiteBshineBstarsBlowBfoolByearsB2BchanceBgroundBwakeBheavenBlotBthinkingBweveBgaveBbrokenBwantedBsinceBwildBcuzBbedBalongBfunBsuchBtalkingBstarBsetBdropBlayBseemsBclubBtriedBredBhideBstrongBsoonBlovesBthrowBliesBwonderBlovingBslowBwindBfeetBblowBmakingBdoesBwitBsaveBimaBpartBmeetBburnBdoingBbornBfullBbloodBhoneyBcontrolByoudBveryBpullBwhosBuBimmaBwordBkillBcarBreasonBtomorrowBpayBfastBkingBbitchesB	christmasBheartsBdoesntBgetsBthreeBstandingBonesBpastBbeingBcatchBaboveBlovedBgettinBdirtyBmoonBfearBwaterBstepBhaBlilBcoolBmomentBchildBeyeBdrinkBwouldntBwalkingBwantsBwallBhairBtiredBsorryBfollowBsaysBanymoreBapartBbothBtakingBtakesBtryinB1BlookinBstraightBbetweenBstreetsBi’mBtightBseaBjumpBwhoaBlearnBuseBladyBsayingBphoneBwarBsitByeaBcallingBfeltBwhateverBgunBpopBuponBbitBcutBeveryoneBmotherBnothinBburningBniggazBsadBshotBrestBlandBtalkinBlipsBbangB	somewhereBseemBhuhBstartedBcominBcaughtBturnedBtrustBafraidBdriveBsummerBmadBfeelinBalreadyBpickBholdingBdancingBsingingBhookBgimmeBpeaceBwasntBdeathBgoldBspendByoursBcheckBbetBacrossBmetBpromiseBbrightBperfectBmostB	differentBniceBtrynaBpowerBspeakBrepeatBnBsonBbreatheBvoiceBpassBmenByetBplayingBdearBkeepsBfrontBblindBnightsBhigherBloverBdaddyBearthBriverBwinBsmokeBringBokayBoutsideBsickBchildrenBnowhereBhalfBbreathBmouthBworthBskinBisntBboomBdoinBnumberBtasteBlivesBlovinBknownBreachBtroubleBfellBcalledBclearBfuckingBmillionBkidsBohhBcryingBhelloBsendBfuckinBoBladiesBbuyBeatBdogBstoryBbrokeBemptyBbluesBlooksBreadBworryBgreatBblameBcarryBgivingBgreenBactByearBwatchingBeBgrowBriseBswearBkneesB
everywhereBneedsBnearB3BdarlingBprayBleadBangelBsexBminuteBfutureBfourBleavingBhoesBturningBshoesBthankBpictureBscreamBloudBlivinBwaysBscaredBgBtrainBmeantBwhichBmovingBradioBspaceBtellingBsecondBrunninBtearBflowBschoolBayBmrBballBclothesBstrangeBshootBfinallyBhangBshareBpussyBwomenBshameBpushBguyBiceBwindowBbelongBcosBfaithBsexyBbreakingBlookedBrichBtheyllBlaughBcannotBehBheatBhoodBfatherBshutBshellBagainstBchangedBeverydayBtreatBjoyBpaperBwarmBfunnyBcloserBsomethinBshoutBwasteBloversBbrotherBlosingBthousandBsimpleBsignBkickBmemoriesBblockBstuckBfatBratherBfadeBcrossBalmostBsomedayBwearBdreamingByorkBwalkedBstoneBknockBsightBstyleBanyoneBfamilyBwoahBbrainBbridgeBcashBouttaBbyeBworkingBshiningBx2BhundredBthinkinBhurtsBmemoryBmagicBspecialBmilesBsayinBjesusBheresBwineBqueenBfiveBmiddleBsBwallsBfillBanswerBlessBdon’tBgladBanywayBshawtyBfeelingsBpointBaheadBfreedomBfewBdoubtBsecretBmeansBsingleBdarknessBdBglassBrollingBsomehowBwideBpoorBokBcmonBwingsB50BmissingB	yesterdayBpretendBbowBkidB
everybodysBdyingBcornerBlyricsBrapBhoBcountBholyBbusinessBluckyBwelcomeBcrowdBwriteBstickBtenBhappenBcrackBmakinBcleanBdickBbrownBmirrorBtrackBcloudsBsittingBdatBtreeBdevilBpieceBprideBchainBbecomeBplayedBstupidBsongsBchooseBangelsBhavingBsafeBwithinBsnowBdjBoceanBanybodyBhangingBheldBmessBlongerBshallBjoeBcriedBkindaBkeyBbrandBbBfakeBholeBdressBfreakBlearnedBdryBnewsBinsaneBcoupleBwaveBgamesBsmallBshadowsBbeginBmaBtreesBsixBdawnBlyingBedgeBnoneBmovinBdoggBwetBwedBluckBheavyBlooseBwestBdiamondsBturnsBit’sBrockinBsugarBthoughtsBhourBcarsBwheneverBdealBkeptBinsteadBpaidB2xBdiedBagoBdeBsilenceBstormBproblemBdoubleBquickBspotBmountainBdrunkBsnoopBstealBfightingBtypeBsenseBwifeBbeyondBbounceBbandBnahBwayneBsurviveBmaryBbottleBtvBdiamondBsceneBskiesBsilverBcakeBsupposedBplanBfreshBquiteBharderBsuperB	searchingBpriceBneededBtakenBsunshineBhandleBooohBdustBlaterBmidnightBdooBgrabBmindsBstartsBrollinBbroughtBcountryBrealizeBplacesBforgiveBhoursBdirtBhumanBguitarBcB	screamingBrhythmBflameBsleepingBcrimeBtwiceBbarBslowlyBhaventBleavesBbuildBjobBstateBchickBfameBfacesBbirdBsouthBlinesBraiseBothersBtripBkillingBcaseBbellsBwalkinBbonesBsisterBbottomBdollarBtakinBweakBaskedBspentBsinBfingersBneckBshadowBridinBhappenedByellowBknowingBwhereverBshedBcoverB	wonderfulBdesireBgloryBboughtB4BlifesBbiggerBendsBevilBgivenBrulesBplayinBgoldenBstrangerBparadiseBbeautyBexplainBbesideBsmellBbustBdrivingBsweatBprobablyBworldsBfactBbookBearlyBanywhereBgunsBghostBtongueBmommaB	everytimeBhoeBranBhopingBfuckedBridingBbootyBspitBsellBsandBfilledBmovesBfitBbossBflyingBchainsBpiecesBjohnBclapBboneBstrongerBstuffB
hallelujahBshotsBgrownBproudBsevenBitllBnakedBdenyBcupBraceBshortyBsorrowBroseBdigBlaughingBmovieBtoughBthangBliftBlockBhillBweekBbassBjackBbringsBdumbBleastBviewBmotherfuckerBstareBproveBlockedBreturnBhighwayBhopBsoftBfigureBquestionBwokeBbuiltBweedBfateBwooBwinterBdeserveBbleedBfallsBhomieBbagBtBrespectBcandyBslipBhungryBsundayBshortBooBmansBsittinBtallBaskingBwrittenBspiritBwhatchaBmiBhatersBlivedBwastedBmachineBgrindBchestBeveningBguysBwhipBfoolsBbirdsBfavoriteBglowBawakeBdarlinBnothingsBcentBprayerBcaresBbeatsBstrengthBnineBseeingBfeedBpackBleanBghettoBdoorsBspeedBlickBfeverBdareBclimbBgraceBmercyBpoppinBmistakeBthaBstoodBspinBboundBsentBescapeBdancinBswitchBflowersBquitBactingBwhisperBbeneathBrecordBtheyveBqueBmurderB	attentionBrushBbaBlettingBshoulderBhollaBpaintBdemBdopeB	wonderingB
underneathBearBwaitinBbottlesBboxBhistoryB	breathingBsurpriseBsoldBquietByou’reBspinningBbelowBlovelyBwheelB	happinessBgangstaB
californiaBbiteBbeatingBkeepingBimagineBchaseBeverythingsBmoBromanceBchangingBwashBhurryBfixBthrillBfairBakonBstaringBtuneBgodsB	disappearBchrisBgrowingBgangBthunderBlatelyBbreaksBteachBohohBblowingBwowBpoisonBheadsBfingerBdeeperBsoundsBmonkeyBswingBstageBspellBproblemsBpleasureBplansBsoloBpicturesBkissesBparkBmessageBshouldntBpocketBmistakesB
girlfriendBdoctorBmiseryBdiBweightBsantaBlawBoooBnobodysBselfBseatBworkinBvisionBbendBworstBrhymeBgrooveBdressedBcrewBrocksBcryinBcatBxBdrownBpulledBhotelBclockBeastB5BsouljaBvoicesBzoneBtaughtBgivesBfantasyBunlessB	listeningBpreciousBlegsB	beginningBforgotBsilentBwroteB	surrenderBkissedBfallinBcallsBageBweatherBbrothersBrosesBmerryBnickiBrhymesBfolksBbumpBroughBnoiseBteamBwheresBworseBbreezeBbeachBslideBletterBfBamericaBjeansBtellinBamericanBbusyBtableBsecretsBkeysBhipBshipBfearsBtherellBlaidBgraveBcan’tBcallinBintroBlevelBtestBdangerBayeB	hollywoodBspreadBplaneBhappensBsavedBchoiceBbreadBwonBshinesBflipB
differenceBpassingBamazingBuhhuhB	satisfiedBmBdistanceBchillBcribBknifeBfasterBwaitedBbegunB	dangerousBsailBshouldveBmorninBsuddenlyBdrinkingBsoldierBdoughBwheelsBpourBarmBshowsBruleBlifetimeBahhBcherryBsuckBcriesBnastyBwiseBbegBbankBremindBjohnnyBthinBjayzBrockingBcrownBenemyBasleepBspringBwavesBmysteryBhidingBsteadyBpoliceBpumBohhhBbattleBpassionBbuildingBlikesBhatBwatchinBpimpBpaBclosedBtipBstoleBhorseBeitherBcloudBnoseBhustleBcrawlBrisingBfreakyBflamesBplusBbearBdimeB	moonlightBkilledBpromisesBbombBmeaningBburnedBminutesBtiedBstoreBsearchBsignsBgardenBsaturdayBsoulsBseesBclassBwishingBfallenBgrandBconfusedBchasingBx3BdrumBbillBpressureBhealBbellBsmokingBbooBfamousBuhhBtenderBolderBregretBpromisedBliedBendingBchangesBtieBfootBmiracleBjoinBgivinBpoundBexactlyBringsBmisterBmarkBtaBwalksBteethBdogsBgreyBremainBmelodyBgreatestBcolorBroofBfleshBjBsmilingBchurchBbeganBswimBfinalBanimalBdecideBtwistBthrewBseemedBjudgeBgrassBcomfortBstartingBcolorsBdestinyBtwentyBrealityBburnsBadmitBmikeBolBheroB	goodnightB
somethingsBsinginBx4BreasonsB	questionsBflightBdesertBcompanyBgasBbitterBwatchedBi’llBfunkyBstoppedBthanksBrainbowBguiltyBbraveBbulletBtroublesBdonBwantingBsillyBshakingBactionBcrashBcertainBmightyBhedBeightBdaughterBmissedBmoodBlonesomeBballinBoooohBblowsB	strangersBlaneBeaseBtruckBthinksBtellsBmattersBkitchenBcalmBbirthdayBdragBhooBheavensBpantsBmotherfuckinBthatllBdollarsBwastingBjamBbringingBpureBpassedBcheapBwearingBcostBnamesBclownBdreamedBcoastBwindowsB	forgottenBcruelBwarningBpathB	heartbeatBbusBdramaBhallBnatureBfieldsBdudeBjealousBfadingBcourseBboatBslaveBsurelyBplaysBgrowsBmovedBbeginsBdowntownBsunnyBbabysBraisedBsteelBtornBthickBstayedB
generationBmicBmasterBlimitBbumBfoBfoodBmmBfortuneBmileBearsBwhoseBswallowBtuBpageBwrapBmomB	heartacheBshowedBmmmBdadBswagBkillerBheadedBfancyBtwistedBanswersBattackBplentyBinnocentBelectricBblessedBhaloBmarriedBendlessBhanginBfridayBshopB	mountainsBcastBwrappedBringingBholidayBdrugBarentBblowinBhungBbeastBhardlyBchancesB	champagneBalthoughB	celebrateB6BlaughterBbopBmadnessB	telephoneBanytimeBsparkBshoreBprivateBsheetsBflashBleavinBhitsBblessBfaultBtrickBlondonByouthBtideB	ourselvesBholdinBhillsBreachingBstationBhonestBbootsBtouchedBrunsBnumbBjailBislandB	somebodysBsmokinBsatBhurtingBadoreBbeggingBgucciB	directionBtheeBstackBpersonBbullshitBtrapBmissionBhipsBhavinBfieldBburninBuglyBtrunkBseriousByBgrewBdrugsBblewBchairBgiftBclickBshyBlotsBactinBthrowingBkissingBbodiesB10BrBmomentsB
lonelinessBalbumBcoatBcardBzeroBwhetherBstandinBboogieB9BsirBstonesB	happeningBdancerBcopsBlistBplainBfloatingBelBstandsBpullingBjimmyBbayByuhBstripBmotionBsuitBstrikeBwipeBwhistleBlouderBcircleBartBsippinBfoolishBsettleB
revolutionBdisguiseBdiesBchicksBnationBmatchBundergroundBweekendBnorthBbagsBweezyBorderBlitBliquorBvainBnoticeBuniverseBshakinBseasonBenjoyBsomeonesBguideBveinsBsmilesBcopBtrippinBstepsBpapaB	lightningBchristBpurpleBcardsBwindsBscarsBloadBundoneBbrickBwoodBnooneBbarelyBrawBprayingBthroatBhmmBjokeBcreamBchaBlessonBblastBhahaBwillingB	situationBreleaseBloadedBpumpBmothersBpBcatsBcrushBheelsBtravelBpushingBpocketsBwristBtoesBthugBresistBduBbarsBwerentBmondayBmissinBfadedBdrowningBhopesBexcuseBcarefulBtrulyBlemmeBconversationBsqueezeBripBbreakinBflagBthruBpinkBexpectBarmyBjaneBplanetBdistantBawBnervousBbomBrappersBbelievedBriderBdrinkinBdiceBcozBowBontoBnoteBmotherfuckersBceilingBcountingB	confusionBbleedingBstacksBoweBopenedBmeltB
impossibleBforceBjungleBbeefByehBrowBfashionBstoriesBliarBbloodyBcureBattitudeBputtingBdiseaseByeBshootingBsimplyBjetBgripBdreBturninBfloBscreenBnaturalBleadsBdiscoBcurseBburyBtickBpillowBexceptBsaintBgentleBworriedBkingsBvictimBsunlightBoftenBmichaelBdateBcompleteBcoffeeBbrighterBbedroomBrecordsBplasticBgalB100BsplitBforthBemotionBdarkestBangryBtracksBmixBeternityBwaBsystemBfailBdipBshirtBbeinBrelaxBholdsBdrinksBbillyBsufferBsingsBdroveBbotherBsoldiersBsangBwitnessBusherBweddingBfurtherBain’tBshapeBharmBcokeBtiBsinkingBcageB	believingBsizeBfishBsuicideBgrayB	boyfriendBscoreBpityBnumbersBlottaBaddictedBtrappedBspokenBpressBpaintedBkicksBhomiesBfliesBcouldveBashesBmainBjacksonBbodysBtouchingBgatherBframeBrefuseBforeignBreligionBoursBflowerBwireBroadsBlazyBlustBbenzBriversBleatherBimaginationBembraceBstopsBrunawayBgsBconfessBcirclesBtoeBthat’sBblownBtexasBbeerBsavingBcuteBxzibitBrestlessBrageBignoreBordinaryBillusionBflowsBhushBechoBdaddysBsipBrescueBfinishBdreaminBcandleBcameraBshouldaBwickedBformBvideoBplayerBjumpinBshadesBstruggleBspokeBscratchBclaimBticketB	followingBextraBadviceBwinnerBpimpinBfunkBworksBsquadBminajBeatingB	nightmareBi’veBironBdivineBmetalBangerBwhooBhammerBappleBwatersBteaseBsinsBsinkBpistolBledBaimBlyinBableBlBrainingBcellBsadnessBmamiBhiBpickedBscreaminBdevilsBaddB	afternoonBmiamiBasideBwreckB8BtheydBshelterBdinnerBheadingBmonsterBheroesBhearingBprisonBmodelBfreezeBemotionsBbecomesB	shatteredBnamedBdrawBwaistBdroppedBcheekBhotterBseekBpoolBsighBbillsBwashedBwakingBsweeterBsoftlyBsmackBsakeBbuckBhorsesBhatinBclosingBdamageBshowingBrolledBmarryBknockingBdancedBvalleyBlikedBcrossedBburiedBdamnedBoughtB	recognizeBteaBrihannaBreachedBlettersBexcitedBwritingBwearyBspareBpairBtoysBenemiesBbulletsBviolenceBproofB4xBsaleB	timbalandBpoleBlaughedBkingdomBbricksBupsideBslippingBrecallBlayingBdumBcrashingBstringsBsistersBneonBhungerBpenBkneeBkillinB	everyonesBashamedBwhiskeyBstressBspendingB	sacrificeBrollsBflatBtraceBsupposeBplannedBherselfBwinningBteBdrankBlearningBtradeB
pretendingBfindingBsmoothBcheatB
summertimeBnearlyBcommonBquarterBforwardBtricksBcheeseBbiggestBstayingBknockinBeasierBe40BawhileBsunsetBmapBenergyBchoseBslayBpouringBdriverBlipBdaylightBclipBbecameBteacherBmagazineBjumpingBhiddenBsnapBsmartBworkedB	hurricaneBdripBdestroyBachingBdollBtoastBdriftingBbubbleBumBthirdBofferBmendBsquareB	shouldersBmentionBhollowBdeeBshadeBfrozenBbrainsBslapBseanBdemonsBtrashBhugBdeckBcutsBthiefBprinceBoutroBellaBciaraBropeBtinyBbabiesBagreeBunknownBtalkedBjuiceBpapersBjointBiiBevryB20BhatingBgainBsuckerBstairsBsixteenBpitbullBfanB
temptationBsometimeBpushedBjayBhopelessBhimselfBhelplessB	desperateBthyBpanicBfistBchromeBweeBstoppingBleBfedBcrawlingBsunriseB
reflectionBplatinumB	innocenceBaffairBsatisfyBprayersBfloatBthrownBdrakeBsmashBpalmBmodernBbirthBsweetestBtaleBspineBrebelBpillB
iâ€™mBheavenlyBboredBspeakingBlegendBfocusBbrooklynBgravityBendedByupBthouBnickBlaceBsonsBsellingBleeBbanksBsendingBrainsBfadesBthroneBlameBbustaBwiggleBtrafficBorleansBnailsBholesBegoBsearchinBpublicBmessedBloBstBnaughtyBeasilyBfellasBamongBlongingBflashingBfckB7B
understoodBseasonsBpaleBclausB	salvationBmamasBgateBtapeBstreamBsharedBpieBfiftyBanimalsB	surprisedBstaysBputtinBlegBoclockBregretsBpositionBdrumsBcoveredBchickenB	butterflyBwhydBsetsBooooBbuttBbooksBremainsBfiguredB3xBtoyBratBchargeBcadillacBacceptBroleBparisBmoviesBexistBtreasureBniggerByardBwearsBcaptainBbeeBusingBideaBbewareBtroubledBrainyBgatesBfalseBrentBdrivinBrumBfoughtBblazeBswayBpennyBlapBitdBbunchBserveBhardestByoungerBtalksBseasBcomplicatedBshookBflyinBrapperBparadeBmarsBrangeB	cigaretteBcharmB
sweetheartBpackedBelsesBdisBurBforeBrefrãoBcookBworeBspeakersBjonesBjamesBtourBstringBlouisBlossBtriggerBlosinBmallBbuddyBdrawnBballsBthrowinBstuntBprizeBcreditBroamBpreacherBlanguageBgentlyBfathersBrubBterrorBsympathyBsuccessBcoupeBpunkBdrivesBcrystalByahBseparateBloserBdropsBboardBwaitsBvibeBtearingBsmiledBscreamsBrollerBjourneyBhabitBfollowedBpresentBalleyBprotectBlastsB‘causeBtowardsBsatisfactionBshaBlullabyB
yesterdaysBpraiseBswimmingBleadingBcarriedBswingingBeraseBavenueBsailingB	sufferingBsteppinBshoeBdingBghostsBcruiseBputsBpotB	affectionBplayaB	importantB
completelyB
conscienceBayoBtrailBspanishBshockBlungsB	invisibleBdyinBdespairBthighsBcolderBalsoBsecondsBseedBsacredBrewindBmonthsBuncleB	presidentBpatienceBjealousyBfillsBcreepBcastleByepBhopinBbandsBwhollBriskBfuckerBsworeBknockedBsolidBmillionsBlackBcocaineB	superstarBrocketBrevengeBjumpedBgypsyBforgivenessBcompareBbareB	there’sBsanBqueensBmollyBdeliverBcomplainBtablesBmaskBkissinBunderstandingBridesBremixBmixedBgayBfansBcoversBsouthernBsleeveBpillsBbangingBaffordBskipBspiritsBmilkBhauntBfaBdunBtastesBstereoBplateBoleBfactsBwisdomBsummersBrealizedBfinishedBecstasyBclueBmacBgrantedBallowBjingleBgeorgiaBenBwouldveBvipBmackBchokeBcandlesBseBmayneBhookedBpeepBlingBguardB	emptinessBblahBwoodsBshoveBexplodeBoriginalBnggaBflewB40BsnakeBlaughsBvisionsBpardonBkickedBdivaB	seventeenBjusticeBhailBfrenchBflowingBchamillionaireBborderBashantiB	sensationB	neighborsB
heartbreakBlipstickBharmonyBdrBbringinB	breakfastBbelongsBexpressBstuntinBsharpBhahBeyedB	apologizeBshelfBpagesBmemphisBdiscoverBcourtBtextBstrutBkillsB	guaranteeBexBchillinBcaredBblissBslippedBmajorBdrippingBdivideBsleepinBnerveBcrowdedBborrowBrubberBkeepinBeveBchosenBayyBaahBvegasBstormyBstolenBsprayBshe’sBprincessB	prechorusBpianoBooooohBeternalBshinyBshadyBfamiliarBappearBsticksBridBracingB	tomorrowsBsoonerBquicklyBneighborhoodBlionBgimmieBfreaksBcollideBbettaBwishinBkickingBfortyBbobBtailBsaviorBhorizonBuptownBlakeBdriftBchopBshoBcracksB
themselvesBmillionaireBhealingBdecidedBcrankBwon’tBwarsBsensesBrepBrefrainBengineBdawgBbaddestB–BunionBnorBinchBenglishBkushBvBruinBcowboyBbluntBteenageBdiggingBcodeBwoundsBpickingBpayingBneitherBmonthBfiresBdeyBcountyByousBwoBlibertyBgreedBunBpurposeBbroadBwolfBtuesdayBsaintsBrattleBpsychoBbelieverBtonightsBslippinBshininBgoodnessB	footstepsBchewBancientBvictoryBumbrellaBthirstyByayBshowerBknowinBcarterBbridgesBusaBroomsBmexicoBrobBrayBhumpBworriesBwomansBvelvetBpaulBfilmBwanderBspinninBcockBbombsBwingBtonyBsitsBbuttonB	travelingBthirtyBslipsBmarchBgetchaBchipsBsteppedBneedingBhazeBbellyBsoberBslickBporBnawBmudBmedicineBgazeBweaponBsleighBperfumeBoilBcommandB	backwardsBstayinBrahBoughtaBinternationalB
governmentBtwelveBpartsBimageBfifteenBweepBsaltB	representBpoppingBoopBmommyBfenceBdudesBfaithfulBcreatedBblindedBallowedB
photographBnevaBeagleBcenturyBvisitBtrippingBstompBconstantBwoundB	whisperedB
tendernessBrightsBcouldaB	automaticBsungBhollerBginBdiveBstrapBridaBrecklessBlet’sBfloodBdizzyBconcreteBblinkBragBkBhBdecemberBdavidBcolourBwhoreB	tennesseeB	spotlightBsidesBhalfwayBfooledBtumblingBshipsBrainbowsBmeatBheartedBgoddamnB	gentlemenBchicagoBsurfinBriotBitselfBcurtainBboldBmariaBmakeupBknoBkittyBhonestlyBhesitateBflawlessBcitiesBbaseBwoohooBstretchBrocBhustlerBfarewellBvoodooBsymphonyBstomachBshiftBmansionBlingerBleaderBindianBdimB
appreciateBswordBsackB
reputationBpreparedBmoanBannieBspeaksBhousesBforestBzoomBwhispersBshownBpostBmadonnaBjewelsBcircusBblankBbladeBtoreBstrappedB
microphoneBechoesBbibleBbegginBstruckBseedsBpissBphysicalBglockB
confidenceBblazingByellBusuallyBsunsBfrontinBtoneBscrewBhighestBduckBdonâ€™tBwishesBtriesBradarBneedleBguiltBeltonByearningBmillBmiaBluvBjustinBdoeBdailyBalarmBtunnelBparkingBlisaBkillersBinformationBdreamerBclimbingBbucksBamenBwornBvacationBstingBsocietyBphantomBohhhhBlendBkellyBfinestBdragonBcheerBaskinBtortureBtalesBshtBrudeBmakerBhasntBbanginBpunchB
incredibleBharlemBdemonBahhhBwishedB
whisperingBuselessBtragedyBstealingBsecurityBpartnerBmailBlayinBgeorgeBcollarBwondersBstumbleBsoreBdrainBcurtainsBsettingBfruitBsippingBgangsterBdamBselfishBdueBcrookedBapplauseB12BnutsBmotherfuckingBhealthBfictionBenterBlosBgunitBcreepingBcaliBswaggerBplayersBpaceBhonkyB
especiallyBduesBsteppingBseatsBreplaceBbutterfliesBohohohBmcBfavorBdroppingBdoomBtwilightBpeterBindeedBdesignBdatsBcomfortableBburstBbrideBweeksBvanBtripleBspiteBsparksBscarBplugBcouchBshowinBsallyBfindsBcottonBcelebrationBbathBtowerBroarBrichesBmotorBmessingBglowingBfiredBeatinBcoloursBawfulBsteamBsentimentalBpipeBfarmB
cigarettesBbentleyBawareBwohBwhoeverBweaknessBstonedBpreachBpeoplesBlabelBhiphopBcamerasBpoundsBleafBjulyBfrownBflavorBbobbyB	starlightBsirensBpearlsBi´mB
intentionsBeverlastingBdeniedBboringBblondieBskinnyBpullinBlawsBjeBiiiBhuntBfairyBcountinB‘emBmaterialBlambBkickinBeyBdoveBclosetBcenterBbacksBateBsingerBscaryBrappinBraBpauseBgreedyBglassesBautumnBsamBgutterBflownBcuttingBcreateBcornersBabuseBwinnersBvirginBsleptBmessinBmeasureB	breakdownBarriveB30BviciousBscentBreceiveBlucyBhonorBboreBthugsBsuddenBposeBmisunderstoodBjiggaBjeanBgutsBgoodbyesB
connectionBcharlieBtiesBsocialBshotgunBprayedBpowBnuttinBjuneBhovBunkindBpressedBpinB
perfectionBdependBscienceBprisonerBlouieBjusB
disrespectB
compromiseBrimsBpresenceBgrillBfailedBesBdestinationBzBwizBslamBseeinB	satelliteBrodeBpassesBdelightBsinnerB	septemberBrailroadBmirrorsBhittinBhauntedBfoldBdoubtsBdeafBcriminalBbeliefBaprilBunderstandsBtreatedBstrayBshootinBreplyBprepareBprayinBparentsBmiraclesBmeetsBhomeyBfkBcapB	boulevardBbeamBwBtossBthankfulBsleepsBrickBmarchingBfuseBdotBbeyonceBappetiteBtoothBtomBcabBbroBupsBsuitcaseBskirtB
permissionBmumBcollegeBbowlBalcoholBrunwayBmuscleBhandsomeBcousinBbarbieBwoohBslimBrazorBoceansBhurtinBfussBdisasterBbutterBupsetBrootsBpuffBlosersBjiveBfourthBdeadlyBblingBbailB
backgroundBahaBhoustonBclingB	availableBweirdBwahBsicknessBmaneBfriendlyBburdenBblessingBtattooBreactionBlatestBinnerBhomesBhearsBhahahaBdestructionBdeepestBacheBunhappyBtaylorBhelpedBfitsBfighterBdecisionBclassicBchasinBmammaBjuicyB
frightenedBenvyBdrewBcrunkBchinaBbrushBaceBzooBwintersB
undercoverBthoBtapBserviceB	knowledgeBjacketBhelpingBfilthyBdon´tBdoggyBbumpinByayoBstrikesBsexualBproBniBhatedBgrinBbuzzBalibiBwe’reBtinBstainBroundsBrippedBpracticeBponBpersonalBlocalBinvolvedBreignBnoticedBjimBhauntingBfightinBdaleBclearlyBburntBwatB	wanderingBstartinBsortBshitsBmovementBinsecureBhowsBgeeBcumBbronxBsaneBdividedB	dedicatedBcharmsB	worldwideBvowBtotalBtockBsurroundBrevealBmistBlloydBinspirationBgirlfriendsBgasolineBexitBcomoBchinBbroadwayBtollBswellBnotesBmrsBlaughinB
hypnotizedB
guaranteedBdisgraceBcontinueBcleverBmassBimpressBgangstasB
constantlyBtankB
surroundedBhelpsBhadntBgrindinBcatchingBwavingBsweptBsinnersB	rocknrollBpatientBkindnessBgatBfucksBcomptonBbathroomBbahBalBwidBwearinBuntoBstoppinBrealiseBhellaBgearB	emotionalBdiddyBdemandB	connectedB	chocolateB	childhoodBbetrayedBwonderinBmoneysBjerkBcultureBchevyBwassupBrareBperhapsBmentalBhittingBdealingBsueBstickyBsightsBshoppingBpeacefulBminesBgotsBdescribeBcrumbleBcrackedBnananaBjordanBhoweverBhatredBfightsBpantiesBobviousBlargeBfuneralB
wonderlandB
televisionBelvisBcourageBcookinBplanesB
nightmaresBhottestBgloveBconquerBcaressBworshipBrossBquieroBmagicalBloopBbuyingBbatBtrainsBrealestB	perfectlyBoohhBgiftsBforgivenBcarpetBadBlistenedBfinnaBdootBclearerBwhodBsellinBhustlinBhaterBfollowsBfemaleBdialBcompetitionBagesBromeoBhornBflexBtigerBrushingBorangeBgoalBfogBclubsBcheckingBwindingBsharingBporchBpickinBnasB
heartachesBflickBbattlesBtouchinBsurvivorBpossibleBnationsBfoolingBdevotionB	decisionsBcolaBcherishBwhipsB	teardropsBsupermanBrabbitBmomsBmeetingBmashB	magazinesBhoundBgloomBglanceBdynamiteBdefeatBtatBsignedBshallowBrockyBpowersBpeasBjarBhumbleBfiBdroppinBbarrelBthangsBracksBjokesBinstrumentalB
experienceBbustinBbushBbucketBbeholdBalienBviolentBriffBreleasedBnoelBmcsB
invitationBfixedBcreationBbloomBschemeBpoppedBnggasBhatesBchampionBwouldaBsuitsBsiBoyeB	eternallyBdreamersBdahBchoirBversionBstudioBromeBrememberingBpushinBnuhBluxuryBkhalifaBflagsBexcusesB	daughtersBalotBwhippinBtangledBpartiesBobsessedBmercedesBkindsByeahhBtumbleBreadingBpullsBponyBpitBoddsBkongBdodoBdegreeBdashBcoreBargueBshiverBphaseBmarketBlisteninBhustlaBfeedingB	christianBchecksBbullBblondeBbillionByellingBwildestBsumBjoeyBjanetBcrackinBcausedBbehaveBxoBthievesBtensionBsolutionBshackBrubyBromanticBragingBkarmaBfrozeBcradleBsneakBrelyBofficeBitâ€™sBideasBdespiteBcopyBcaneBbeltB45BtranceBohohohohBohoBmothaByerBwarmthBpoundingB	permanentB
forgettingBfacingBchillingBbentBbeepBafricaBsidewalkBlangBlalalaBladderBinkBglimpseBeminemBcusBcloudyBsocksBpartysBnailBhallsB	conditionBweaponsBpedalBnopeBguitarsBdenBcliqueBcarelessBastrayBterribleBsheepBsendsBscareBreverseBpointsBogBmusicsBmornBjunkBjudgmentBjellyB	introduceBbarkBstashBsidewaysBshieldBsaluteB
protectionBmariahBlessonsBhonestyBgrainBfedsBuntrueBtrustedBtpainBhidesBheeBgiantB	forbiddenBchaosBtopsBspillBratsBparanoidBknotBgreetBglitterBfillingBaverageBwealthBshimmyBrottenBnarrowBfactoryBduranBdefendBbernieBbeatinBuptightB
travellingBshoutingBlunchBloyalBdegreesBdancesBbroadsBbouncingB911BweepingBwebBpissedBniggersBmmmmBjukeboxBfuelBfifthB	fantasiesBdroBbtchByonderBmississippiBliftedBkanyeBjaggerBit´sBgyalBgetawayBempireBearnBdetroitBblocksBbeamsBtripsBsolveBmeltingBkimBgriefBfatalBditB	confusingBbasementB
attractionBwilliamBtrialBtonkBtanBtameBsuckersBpavedBmodelsB	mistletoeBlesBgunnaBfeatherB	favouriteBelevatorBcareerBbikeBwillieBtwerkBsurfaceBounceBhowlingBgreaterBfacedBenglandBdigginBdealerBalabamaB…BvoidBtikBspendinBraisingBpopularBnellyBnanaBmealBlowerBhenryBg6BdebtBatlantaBwackBuhuhBtonguesBtipsBstarinBsleepyBromanBjonBglueBfragileBfeatB	creaturesBbritneyBbreathinBapeBwarriorBunitedBrelateBraahBqBmistyBconsiderBceaseBadamBwhineBrhondaBremindsBovercomeBlinkBicyBfuturesBdesiresBbugBboothBvineBthumbBtastedBmuthafuckinBivyBdoorwayB
dancefloorBcowBchillsByou’llBvestBstrungBsectionBrouteBrapeBpacBglobeBdungeonBchronicBappearsBsweepBstormsBpositiveBpopsBgroovinBgrindingBdrownedBdownsBcreepinBclayBwashingBsofaBpresentsBpavementBpatBpaintingBloneBgloriaBbulletproofBbodiedBbalanceBtaxiB
strawberryBstrandedBstaticBreBhusbandBfreewayBflamingB
eventuallyBcornBcheckinBaccountBtickingBthreadBtendBprojectsBelevenBbotheredBtemperatureBstrokeBstakeBsmellsBsailedBpulseBpleasedB	patientlyBmobBcreatureBcompeteBbetchaBwantinBregularBpitchB	passengerBmartinBkinBjerseyBjennyBevaBdippinBchickensB24BunbreakableBsootheBslangBsentenceBscrewedBlamborghiniB	championsBweaveBshuffleBlasBflipmodeBblossomBblindingBakBtragicBswinginBsupportBsourBjamaicaBbrassBaboardBwarnBrunnerBpumpingBprimeBpadBmateBlimitsBlightingBeffectBcrimesBborrowedBbeesBarrivedBtaupinBspoonBsanityBreliefBpeelBmotelB	emergencyBcoughBchoicesBaddressBwonderedBvogueB	swallowedBspiderB	raindropsBneathBmistakenBlandsBjsBgoshBg5BfoxBfountainBfellaB	entertainB	destroyedBcutieBconBcheeksBwoundedBwinsBstaresBoverseasBmachinesBhidinBgottenBfolkBdianaBcheckedBamazedBwhaBvillageBtighterBsauceBrobinBreplacedBreindeerBmarieBllBkneelBgrabbedBevidenceBdangBwatchesBthrillerBproperBmonaBlollipopBkeeperBwhatchuBsteveBsealBragsBpowderBindependentBholBfascinationBdrivenBdrippinBcoalBcarryingBtonBsmokedBsatanBsafetyBnervesB	judgementBhappeninBfurBcurbBcruisingB	brilliantBbreastByouâ€™reBpleadB
fingertipsBearnedBdarkerBcdBboatsBbeastieBamongstBtargetBsodBsmilinBsiteBlocoBhumBhidBdrawsBcautionBbondBblaB	wednesdayBwaxBtailsBpatronBpapiBfiguresBdisappearedBcrowdsBcheatingBbustedBbeatenB	valentineBtokyoBthirstBsweetlyBslumberBshakesB	louisianaBhellsBfullyBdullBdissBcoasterBbyebyeBvenusBtowardBthreatBsodaBsavageBrustBflossBdewBconnectB	blessingsBasksBughB	tremblingBtossedBtempleBsnitchBsilkBrodBplantBfailureBcontactBchopperBambitionBactuallyB99BwesternBsurfBsuedeBsatinBsandsBlettinBkillaBguessingBcupsBcoffinBcancerBbecomingBwestsideBversaceBsprungBsliceBsincereBsealedBpoursB
incompleteBfootballBfoolinBdenialBchokingBunfairBsnatchBreppinB	reminisceBremedyBownsBneedlesBjerryBhustlersBharryBfranceBfordB	expensiveBenginesBdomeBdomBdefBcookieBbastardBwillowBvaBpreyBphotoBliquidBlighterBlawyerBkrazyBguidingBguestBgooBdignityB	difficultBdecayBcraveBamountBwhat’sBthursdayBstrollBscarfaceBrootBmarksBgrowinBcloverBbreezyBbelievesBtwinkleBshellsBrockedBraysBprovideBleagueBjustifyBjulietBjazzBhumanityBgooseBdreadBcopeBcarnivalB	buildingsBwastinBwaltzBtittiesBtattoosBstrippedBscenesBpoetBphonesBnonoBmaintainBlanesBimagesBhayBfellowBeastsideBdimesBcollectBcarriesBbreedBtunechiBtownsBteachersBsugaBsucksBstockB	rearrangeB
possessionBnonstopBmissyBhummingBgratefulBgottiBcustomBbattlefieldB	whistlingBthrillsBstankBsailorBrapsBrappingBpanBnooseBnaiveBmsBleadersB	landslideBhersBgroupB	gentlemanBflippinBcuriousBbonnieBangelesB22B21BwinkB	sleeplessBsampleBquickerBpriestBnorthernBmediaBidiotBhospitalBhazyBequalB	endlesslyBdozenB	concernedBbuttonsBbiB	vengeanceBpuppetBnotionBniteBmugBmouseBlawnBi’dBheedBheartbreakerB
breathlessBbluntsBamorBvirginiaBtiresBspotsBspittinBschoolsBrearBnickelBjaBindustryBhornyBgatheredBgarbageBfeedbackBdollaBcountedBclipsBchBawwBattachedB	apartmentByeahyeahBsuckedBspeedingBspeechBritualBrejoiceBprogressBpimpsBhoppedBheightsBgarageBfocusedBfakingBdestinedBchiefBbunBbrokenheartedBbayouBballoonBarrowBanthemB	worthlessBwarnedBwalletBtouchesBsupremeBstudyBstrainBsnakesB	scatteredBrumorsBpurseBpreferBpaysBpaneBovertimeBnuthinB	ignoranceBguapBgreaseBdeedsBcriticalBconcealBbrianBbitesBwhomBturntBtagBstirBstatusBstabBscriptBroyalBroverBjapanBgreenerBgoodiesB	evolutionBeggB	deliciousBcrowBcripBanyhowBvodkaBsubwayBsearchedBplanetsBnonB	manhattanB	limousineBhangsBfabulousBdespiseBdealsBdanBbambooBappealBwe’llBtwistingBsowBsoundingBsighsBquietlyBpleaBphreshB	paralyzedBouterB	obsessionBhomeboyBflopBferrariB	existenceBetBdressesBdoseBblanketBaveB17BthornB	sweetnessBskysBpouredBplatesBpileB	overnightBoohoohBolaBmoldBlolaBinsanityB	heartlessBdyouBdesperationBdefenseBdeceiveB	apologiesBveilB
throughoutBsweatingBscarredBreplayBpumpinBmaxBinviteBhundredsBfckingB
dedicationBcoloredBcestBbangaBariseBannaB	returningBpoliticsBpartnersBpalaceBofficialBloosenBillestBfailsBdumpB
disappearsBchimneyBtempoBplacedBpainsBnecklaceBlacBjewelryBjawBfrightBfreezingBexpectedBdsBbruiseB1stBtotallyB	terrifiedBservedBsailsBrockstarBnestBhoppinBgumB	fantasticBeggsBdoctorsBcomputerBchiBcentralBbragBbananaBtaxBstripesBstarvingBsmithBscarletBpawnBnetB
mysteriousBmorningsBmobileBleapBlaserBhawaiiBfloorsBdustyBdraggingBdoopBdannyBcruisinBcrashedBcookingBchanginBcaveBboyzBavoidBtravelinBthornsBtenderlyBteeBrichardBmuddyBmileyBlionsBlickingBlampB
hesitationBhamBexploreB	deceivingBclimbedBcheatinB	boomerangBbackyardBworthyBthongBsurvivedBproceedBplotBnutBmonstersBlinedBknivesBisraelBhunterBhopedBgoonsBgeniusB	gangstersBdisagreeBdigitalBclownsBvictimsB	southsideBscreamedBsaviourBpetBoverloadBnowadaysBmustveB
headlightsBglovesBfeastBetcBeighteenBdependsBdaydreamBbetrayBbastardsB
wildernessBstrawBpigBlimeBhypeBexchangeBdrummerBdrawingBdontchaBcrushedBconfessionsBcapturedBbirdmanBbiggieBbbB2ndB25ByrByeahhhBthirteenBspeakinBshooBrangBoutlawBlightersBhollyBgladlyBgambleBfrustrationBfiendsBfartherBcravingBcorrectBchuckBbumpingBballerBvivaBversoBupstairsBtraveledB	spreadingBsorrowsBrelationshipBpearlBooohhhBnearerB	meanwhileBgossipBfrankieBdubsBdaisyBtwitterBtorchBtestifyBsurvivalBsuggestBstackedBseekingB	righteousBpossiblyB	pleasuresBoopsBnormalBlamboBhitchBhandedBgradeBgloriousBfiendBchipBcarolineBbesidesBbakeBbackseatB	advantageBwoeBwatchaBwanBthrowsBsavesB
resistanceBrateBovaBnooBmaleBlouBgorgeousBcausingBcastlesBbindBartistBwriterBwhatdBtuckBsparkleBsnowmanBshoppinBpostedBnewbornBlordsBlogBlalalalaBjudasBinnB
hopelesslyBfrankBforsakenB
discoveredBdadaBbuyinBapproachBamnesiaBwhippingBtrynnaBservingBpoBoooooBmazeBhowdBgroupieBfreakingBflirtBfareBdoggoneBadrockBunlockBtowersBtimingBtellemBspainBsignalB	sanctuaryBrippingBopensBjobsBfistsBchemicalBbidBbadlyBwitchBsweatinB	stretchedBspyBskillsBreflectionsBpoloB	paparazziBmathBkonvictBignoredBhometownBhikeBguruBfeedsBdaveBbruisedB	blackbirdBarmorBapplyBahahByellinBwrongsBtommyBstoveBsoakBplayboyBmouthsBmellowBhowlBgitB	gatheringBforcedBflashinBaliciaBaiyyoBafarB16BtreatsBspeakerBridersB
muthafuckaBheadinB	graveyardBfamBentireBditchBdelayB
definitelyBdawningBcombBwillinBwhysBwakesBunfoldBtamedB
strugglingBspoilBspinsB
situationsBscoopBsaddleBrisinBpumpedBpowerfulBofferingBnuffBmonBmissesBmadlyBissuesBhornsB	evrythingBcursedBchapelBbraB	abandonedBwiserBtalentBstylesB
speechlessBrobotBrisesBriceBpercentBobeyBnateBliftingBgamblerB	educationBdubBdrunkenB
depressionB
borderlineB15BvenomBthickerBsourceBnicoleBfmBexpectationsBentertainmentBdosBdealinBdazeBcrawledBcheatedBbmBblindsB	bethlehemBaliceB
absolutelyB
worthwhileBsurferBsoapBshabbaBraininBprofitBpapaoommowmowBopinionBmcaBmassesBlalaBfuryBerasedBdocBdidn’tBclawsBbelievinB	aftermathBzipBvolumeB
vibrationsBsuckaBstainsBskateBrowdyBrotBproductB
presidentsBhighsBhe’sBfergieBdrillBdriedBboBbittersweetBbeansBbasicBaudienceBwhoaohBunitBtreadBticktockBsupplyBrugBreturnedB
neighboursBmickeyBmessiahBillegalBhallwayBdwellBdancersB
crossroadsBcasketBbouncinBawesomeBahhhhB101BveinBvanityBvacantBundressBtwinBtimerB	temporaryB
tambourineBporscheBpatchBopportunityBneighborBmortalBdonchaBcontentBcomplexBcommunicationBcitysBcarouselBboozeBbeyoncéB18ByeaahB	whateversBtemperBsweetieBstripperBstarryBschemesBsadlyB
rememberedBrackBparkedBoxygenBmaidBliBlevelsB
invincibleBheelBgownB	fairytaleBdollyBdemandsBcrossingBconcernB	challengeBbroomB	adventureBtemptB	surroundsBstickingBskullBshooterBseventhBretireBretardedBprofessionalBpoetryB	nighttimeBmonkeysBmodeBmaybachBleaningBinspiredB	inbetweenB	impressedBharvestBgrandmaBdeeplyBclutchBcan´tBbrothaBx8BvoltageBtrillBticketsBthemeBslidingBshhhBscaleBsaBronBprojectBpigsBphotographsBmilkyBlocBlemonBinvitedB	illusionsBhappilyBevrybodyBdugBdougieBdooooBdishesBdineBcorrinaBcindyBbrinkBboiBwidowBstuntingBstatesBringinBrepeatsBpilotBpartyingBnationalBminorBmilBmelodiesBmeadowBlootBhunBhairsBfinerBdreamtBbussBbsBbladesBactionsBunusualBtoolBstrideBreapBpassinBnurseBmediocreBkhaledBinnaBgoddessBconvinceBconcentrateBchatBbizB	vibrationBupperBtodaysB	suspicionBstealsBstaredBreactB	pricelessBparaBobsceneBgymBforcesBflawsBdressingBdeuceBdefineBdearlyBcupidBcuddleBcooBbrakeBansweredBstakesBsportBsilentlyBseveralBreelingBprollyBpoisonedBnoonBmistressBknocksBinspireBhatsBgravesBfetishBeatsBbuysBbranchesBbledBbenBagonyBactsB2000Byou’veBwiresBwhoresBundertowBsonnyBsolitaryBsoakingBshoneBsharkBserenadeB	sensitiveBroutineBrewardBplanningBpeachBneedinBjokerB
frustratedB	crumblingB
confessionBchineseBchicoBchapterBballingBalmightyBwilliamsBsixtyBreelBraptureBpomB
persuasionBpackingBmoonsBliberateBislandsBinfinityBhosBhardcoreBgroovyBfeathersBdjsBdealtBbabylonB
atmosphereBaimingBaightBadvanceBtoolsBswingsB
swallalalaBsuiteBshatterBronnieBrequestBpurpBpoliticiansBplainsBmulaB
masqueradeBkiddingB	interludeBinterestBhuntingBhandfulBexcitingBchainedBbowedBboundsBbinBbathingBwoahohBwheredBunbelievableB	thousandsB
scratchingBribbonsB
rendezvousBreadsBjdBinstantBhealthyBdesignedBdefensesBcrisisBconversationsBcontrollingB	committedBchingB	childrensBchairsBcafeBbettyBareaB11BwolvesBtchuB
substituteB
ridiculousBperBpayinBnormallyBnineteenBlightninBlemonadeBigniteBgagaBfulfillB
foundationBdraggedBdixieBbelongedBasianBunrealBultimateBtrucksBtentBteddyBstainedBriddleBpokeBopeningBmostlyBmixtapeBlordyBglideBdonnaBdaytimeBdameBbannerB3rdB123ByummyBwoodenBwigBunsureBsoarBsmarterB	prettiestBposterBooooooBmojoBlocksBfoeBdivingBdisturbBdenyingBcanâ€™tBbrowBbitsBairplaneB
weightlessBtrembleBstrifeBsettledB	naturallyBlandedBinchesBhennessyB
friendshipBdeucesBcribsBcrawlinB	convincedBcircumstanceBblazinB
adrenalineBachesBabusedBtrampBtheredBsolitudeBricoB	repeatingB	quicksandBpovertyBphoenixBpettyBpainfulBoverdueBofferedBmaidenB	jailhouseBinventedBinternetBhearinBflowinB	deliriousBdadsBcitizenB	celebrityBwormBuuhBuhohBtrophyBthusBsuckingBsoilBsavinBrussianBquestioningBmessagesBlongestBknightBjuryBjudyB	fireworksBerBcuckooBbillieBacidB305BwiredBvanishBtrayBtattooedBshiversBsecretlyB
revelationB	pretenderBpineBpeakB
particularB
motivationBlingersB	lifestyleBjoleneB
impressionBhmmmBgroupiesBganjaBfeeBedBdylanB
directionsBcomplainingBcommitBcoinBcockedBcaringBbedsB	bartenderBwussupBuniformBtipsyBretreatBpartedBpalmsBpackinB	obviouslyBnowsBmoralBlethalBgramsBfreakinBdriftedBdaeBclingingBchasedBchamberB	centuriesBcentsBbittyBandyBwagonBviewsBversesBundoBturfBsweaterBstickinBsteerBpinchBpahBoutlawsBoakBnonsenseBninaBnegativeBmtvBhummerBfarmersBeddieBcriticsB	criticizeBconsequenceBbrewBbloodsBalikeBunholyBtunesBtsBtreatingB	substanceBstevieB
springtimeBsellsBribbonBpuzzleBpourinBordersBoohahBomBmildBjesseBjenniferBitchBissueBhugginBhorrorB	headlinesBgorillaBgoodsBgeneralBgaBdesignerBdeclareBchannelBbearsBabcBwristsB	waterfallB	underwearBtitsBtingBtearinBsufferedBspittingB	spaceshipBsoupB
silhouetteBshinedBshapesBrustyBruinsBpokerBplantedBoptionsBnobleB	necessaryBlobbyBlegalBgullyBgenieBfileBfakinBendureBencoreBdarnBdammitBcycleBcrimsonBconfuseB
cinderellaBcasesBcartoonBbrakesBarrestBabsurdByalaBwhackBwashesB	they’reBsyrupBstorysBsmellingBropesBrodeoBrewearB
playgroundBpeteBmysticBmeekBledgeB	franciscoBcurveBcondoB	christineB	chemistryBcaptiveBbruceBanacondaBvampireB	touchdownBtidingsBsmallerBslackBruledBpossibilitiesBmaniacBeatenBdutyBdroughtB	dimensionBcountsBcoatsBclackBcheddarBcharmingBbrightenBbleekBbitingBawakenBashBalibisByoüBwifeyBwhoahBvieBunwindBukBtryinaBsucceedBsmashedB	slaughterBrobbedBrifleBremoveBpunB
passionateBpalBlowsBlegendsBjaiBgreasyBelectricityBeagerBdrawersBdirectBdarkenedB	carefullyBbrightlyBblurBblackoutBbettingBappearedBakaBviceBvertigoBseamsBscaresBsailorsB
redemptionBraggedBoddBleashBjoanBi´veBhoorayBhabitsBgraspBgospelBfunctionBfloridaB
expressionBcurlBcrackingBatticBatlBtygaBshaneBrecipeBphonyBownedBmotionsBludacrisBleakBkiteBjetsB	jerusalemBjagBhostBgrantBforsakeBflickerBfaintB	enchantedB
determinedBcréditoBcrapBcloselyBcandlelightBbumperBbrazilBbandanaB	aeroplaneBaccusedBtracesBtagsBstealinBspreeB	someplaceBsneakersBrioBrestingBrachelBpenniesBpatterBovenBofficerBlatinBimpalaBformerBeuropeanBdisplayBdeceivedBcodesBchoosingBchelseaBbullyBbamB808BweighBwanderedBtoteBthBtennisB	stumblingBstallBsheetBrickyBpistolsBpipesBosBltdBlifelineBlegacyBjoinedBishBgotchaBflyerBcatchinBcapitalBwindyBunityBuniteBtshirtBticBtechBsurgeryBstunnaB	strangestBspiceBsingersBseizeBrevolverB
restaurantBreportB	politicalBpleasantBpeggyBneatBlandingBkansasB	intentionBhireBglamB
definitionBdedicateBdanielBdallasBclosestBclawBcheersBbeggedBwombBtunedBtubeBtroublemakerBswerveBstatueBsosBsofterBshoresBsecureBsaltyBrevealedBremoteBpointingBperformBmesBmastersBlikelyBfloatinBcomparesBchalkB	certainlyBbubblesBbluebirdBanxietyBalightByknowBx5BwreckingBusualBthumpBstumbledBstiffBslowerBsighingBshowersBshirtsBrichardsBpuppyBoptionBhooksBhmBhappierBgunshotBfreightBfamiliesBcurvesBbunnyBbugattiBbluffB	authorityBwildinBvuBvalueBtireBtimBsubjectB
straightenBskillBsantasB	protectedB	potentialBpiesBnaturesBlyricalBlicksBlearninBjeepBfryBextremeBcreepsBcockyBcoachBchariotB	certifiedB	bubblegumBbarkingBbadderBbaconBaccidentByou´reBwizardBtubBtrustingBtattedB	stockingsBstanceBsockBskrrtBmothafuckinBmankindBliningBhealerBfrostBfoulBfittedBdutchBdemiseBcaptureB	brightestBbonBbicycleBbeachesBamendsB	addictionBxxxoBvowsBtweetBtrumpetsBtombBtallerBstressinBspringsBsportsBspacesBschemingBrelationshipsBrandomBpradaBoppositeBmattressBizBhennyBhehBgenuineBflexinB	fantasizeBechoingBdiscussBdelicateBclocksBchordsBcannonBcampBbudBboltBbalconyBaugustBwaitressBvillainBvanillaBtwinsBteardropB	strippersBsheriffBshakenBsanctifyBrumbleBreliveBreachesBpicksBpharrellBmarleyBlunaticBjuniorBhypnoticBhugsBhealedBgainedBdifferentlyBcoldestBchordB	blueberryBbashB	australiaBadmireBweekendsBtossinBtitleB	thereforeBstackinBslightlyBrumoursBrockerB	rejectionBreflectBpossibilityBpossessBpinesBoperaB
ohohohohohBmottoBmosesBmarysBkevinBkentuckyBholidaysBhealsB	halloweenBfreedomsBeroticBeightyBcostsB	confidentBcheBcarvedBcarolinaB
bitternessBbarefootB8xByachtBwhitneyBwenBwarmerBtunBstutterBsteakBrecoverBqualityBpinsBoooooohBnecksBmarbleBmahBlobsterB
liberationBlaysBjewelBinhaleBgamblingBelevateBdohB	depressedBdealersBboppinBbootBballoonsBballersBaliBwatagatapitusberryB
valentinesB	tombstoneBthugginBsusieBsundaysBsolarBsleevesBrepeatedB
regardlessB	recogniseBpiperBpersonalityBowwBnovemberBmommasBmarvinBkuruptBinfiniteBinfatuationBheightBharborBgearsBframesBfoesBestaBeaglesBdejaBcuttinBcousinsB	copyrightB
contagiousB
commitmentBbugsBblastinBangieBwivesBweedsBvoteBumaBtowelBstoresBspilledB	riversideBransomBqueenieBperoBlimbBjointsBgutBgrimBgravyBfirmBfiddleBfeatureBeveninBdongBdicksBbenchBauldBachieveB4thByaheyBvocalBvalleysB	universalB
technologyBswaggBsilhouettesBsendinB
sacrificedBrehabBrefusedBracinBpoetsBpathsBnuclearBmoiBmereBjulieB
introducedBhelterBglobalBflauntBduskB	dependingBdeanBdadeBcounterBconsequencesBchoppersBchefBblockinBassassinBarmedBx6BwipedBtuesdaysBtuckedBtorturedBtabBswitchinBswitchedBstationsBslavesBshootsBrosieBrobertBracesBpointedBoverdoseB
outrageousBninesBneBmarkedBlistensBlacedBjasonB	intuitionBhtownBgoodyB	forgivingB
footprintsBelephantB	doesn’tBcharityB80sBurgeBtwosBtupacBswornBsweatyB	spiritualBskelterBservesBscopeBrainedBpassionsBoffersBitlikeBhullyBhauntsBflightsB	explodingB	dreamlandBdeedBcagesBarabBamberB13ByankeeB	uncertainBtoxicBtippyBtightlyBswiftB
suspiciousBsneakingBruinedBreloadBraveBpressingBphillyBnyB	marvelousBlickinBi’maBhogBhaulB	formationBfloatsBflippedBfishingB	expectingB	exceptionBdummyBdazB
comprehendBcatchesBberryBanticipatingB500BwhirlBunspokenB
timberlakeBsirenBrulerBpropertyBpacksBobaBmenaceBmemberBladBlaborBkurtBhomicideBgrannyBfrontingBdovesB
disconnectBcurrentBcontractBchartsBbetrayalBbehaviorB	autographBanewBactressByoyoBwhoveBvulturesBtypicalBtimberBtidesBtextingBtequilaB	switchingBsuburbanBstackingBpuertoB
publishingBpimpingB	penthouseBoathBninetyBmaydayBjanieBhookerBgatsBexoticBeffectsBdeathsBbustingBbuckleBboxesBbottomsBbostonBbasketBbaldBaisleBaiBwinninBsupaBstreakBstewartBspoiledBslyBslappedBrougeBpledgeBpizzaBpeepsBnotchBlaiBhangoverBhammersB
earthquakeBcowardBcookedBchamillitaryBcallerBbligeBbleedsBbeardB
basketballBâ€”BwoofBwhoaohohBweakerBwarriorsBtradingBsuperstitiousBsoakedBshBservinBrosyBrazorsB	possessedBplayasBphraseBooooooooBmarriageBmarilynBjerichoBjaggerkBguessedBgrieveBfreedBestateBdrawerBdollsBdisappearingBdeservesB	deliveredBbendedBbatteryBéByumBwillyBwendyBtkoBsurroundingB
supersonicB	supergirlBstressedBspillingBshiftingBroyceB
reproducedBrebornB
productionB	pollutionBpitterB	otherwiseBmystifyBmuthaBmirageBmeterBmajestyBlimbsBleaninBladysBjelloBhastaBgoalsBgalaxyBfriscoBfresherB	fashionedBessenceBdotsBdodgeBdismayBdiggaBdenimBcreekBcookiesBcontainBcomparedBclydeBcleaningBchooBcapeBcainBbaggageBarcadeBamericasB21stBwrathB	womanizerBvocêBtrousersB	travelledBshockedBrooftopBrollercoasterB
punishmentBprintBpirateBpilgrimBoutaBoperatorBmustaBmusicalBlocaBjoiningBjeezyBinseparableBidentityBfrighteningBflingBfishesBfavorsBfannyBfaggotBdriftinBdepthsBdeclineBcoolerBchillyBblushBbaitBanybodysBadiosBzombieByiBwraithBwetterBweeohBwadeBunstoppableBtwasBtoniteBswollenBswineBsupperB
stretchingBspunB	shepherdsBsceneryBsavoyBsaladBruggedBprophetsBpinkyBpasBpackageBownerBooooooohBmanageBlutherBlastingBjadedBimmortalBhelpinBhallowedBfuBfrealBforkBexposedBexciteBcurtisB	countdownBcomB	clutchingBchanelBcausesBcableBbriefBbeliefsBbaseballBassumeBapproachingBanchorBairportBactedBabandonBaaahBwhoopBvideosBuniqueBtruthsBtiminBstitchBstairwayBsniffBskylineBruffBremorseBprovesBpeachesBpatrolBomarionBmurderedBmjBmilitaryBmasBmannersBlawyersBhysteriaBhahahahaBgogoBglistenBfrogBformsBfarmerBfamineBdoobyBcubanBcliffBchimeBchildsBcaddyBbruisesB	breakawayBbratBbendingBassesBainBtreyBthighBswishBsuckinBslutBshovelB	scratchedBscatterBrosemaryBportBpatheticB	parachuteBohhhhhBmopBlamesBirishBinclinedBholaBheadacheBgazingBflippingBdeskBdenisBconfideBchumpBchargedBbuildinBbadoomBamyBagentB23B0ByeaaahBwoooBwitchuBtúBtropicalBtrickinBtribeBswimminB	submarineBshirleyB	respectedBreminderBrapedBpsBpreBpaybackB	mysteriesBmundoBmealsBluckiestBliarsBjoyfulBjapaneseBivoryBfluBfailingBdietBcuffB	corporateBchuBbrotherhoodB	boardwalkBannBactorBaaB1000B’boutBwhippedB	westcoastBwavedBtermsBtellerBstreamsBsinkinB	separatedB
satellitesBsantanaBrogerB	rocafellaBratchetBprowlBpotionBpetalsBozzyBohioB	notoriousBnodBnagBminusBlightlyBjunkieBjtBjokingBjiggyBjakeBiâ€™llBgrudgeBglamourBfloyBflockBfinBfatesB
excitementBexcelsisBexampleBedenBdoodooBcharlesBbuffaloBbetsBbailaB	backstageB	avalancheBarrowsBarrestedBaidBunableB	travellinBtopicBtisBthrowedBsuckasBstrictlyBsoundedBskinsBrubbingBrocketsBreverendBpumpsB	pressuresBpraisesBplagueBpatternBpartingBoklahomaBmultiplyB	miserableB	marijuanaBlucilleBlooBkkB	isolationBhomeworkBhollerinBhoboBhippyB
hesitatingBherbBglareBgiddyBfishinB	fireplaceBduttyBdockBdiaryBdefeatedBcrossesBclaimingB
boundariesB	bloodshotBbananasBauBaglowB	afterglowBtintedBtimelessBthreesB	surprisesB	suffocateBspadesBskiBsharksB	relationsB	reflectedBprophetBpleasingB	overratedBnoticingBmusclesBmethodB
medicationBloyaltyBlogicBloanBlearntBjohnsonBjockB
indecisionBhunkBgraciousBgirlieBforevermoreBfireballBevermoreBdonkeyBdishBdigitsBdidjaBdetailsBdandyBcueBcrewsBcreatingBcoolinBconsumedBblacksB	blackenedB	beethovenBbakingB
backstreetBbackingB	accordingByaselfBwutBwakinBunhBtroughBthriveBtestingBtbirdBswervingBspiralBsimonBshapedBscandalBsaddestBremindedBreachinBpresidentialBpotatoBpoppaBplowB
philosophyBoverheadBmurdaBmothB	moonshineBmilliBmeltsBmasterpieceBmartyrBmachoBlindaBlaylaBinstinctBindependenceBimaaBhomelessBhawkBhatninBgraffitiBfoursBfeaturesBfearlessBdistressBdampB	countlessBcocaBcandymanBbugginBbruhBbrimBblizzardB	blindnessBbitchinBbelleB24sBwormsBturtleBtherapyBsundownBstoopBstadiumBsplashBsewBscrubBsarahBroaringB	remindingBrejectBpoomBmmmdaBmentallyBlewisBkittenBimproveBgiantsBflushBdriversBdoradoB
desolationB	conqueredBconcertBcomaBbuzzingBassuredB	amsterdamBwitchaBwhoooB
undefeatedBunbrokenBunafraidBtwerkinBthumbsBtangBstableBsprinkleBsooBsettinBsabesBroBrealerBravenBquestB	prisonersBpoutBooohhBmurdererBmiceBmatteredBmaeBliftsBlicenseBlaundryBinvitingBfruitsBfreelyBforeheadBfiestaBeeBdatesBcompassB
collectingBcoleBchildishBboominB	blacknessBbatsBbarnBawkwardB	attractedBassholeBapesBamazinB	alligatorBwaanBummBtrainedBtenseB
successfulBstungB	stillnessBspellsBsleazyBskinnedBshoutsB	seductionBscumBrouletteBribsBremyB	remembersBpusherBpssyBpencilBoutfitBnikeBhoneysBhillsideBfredBfootworkBfondBflossyBfleeBflashyBfattyBdrivewayBdragginBdesertedB
compassionBcollapseBclothBcaesarBbtchesBbrawlBbathedBamazeBafricanB44ByeaaBworkersBwhaleBwageBvibesBunsaidBtaintedBsteepBstampBsoughtBsassyBrudolphBrooftopsBpuckerB	positionsBpopcornBphilBparallelBparBobamaBmontanaBmamisBlotteryB	legendaryBjubileeBinsidesBimaginedBfourteenBfixingBcrushingB	crucifiedBchimesBcabinBburstingBblondBbadgeB	ambulanceBworryingBweepsBwavinB	wastelandBunseenBtwirlBturkeyBteenBswordsBsunkBstudentBshackledBsavorBrumourBrubbinBricherBpunishBpriestsB	preachersBpeppersBpabloBonehorseBoerBmoaningBlarryBjudeBjacksB	imitationBgrabbingB	frequencyBfleetingBdukeBdragsBdesperatelyBdeoB	deceptionBdamagedBcrucifyBcombinationBclassyBcattleBanxiousB	afterlifeBwreckageBunawareBtwistinBtriumphBtrendBtradedBteasinBswanB	strongestBsplendorBsortaB
snowflakesBslumsBsigningBsidekickBshoutedBsemiBseduceBrowsBrailsBpicketBpearlyB	paperbackBobjectBnikkiBmythBmowBmournBmondaysBminkBmatchesBkeenBicedBheroinBgoldieBgleamingBgiftedB	fortunateBeuBdebbieBcussBbongBawokeBarrivesBapologyBadriftBaddictB14BzaneBwyclefBwarmingBwalkerB
vulnerableBtyBtippinBtakerB
superstarsBstubbornBslashBshrimpBshowdownBshockingBseldomBscreensBsaraBrumpBrespondBrepairBpregnantBpossumBoldestB
motherfuckBmichelleBmascaraBmagnificentBlumpBlovelessBlouiseBlinenBlimoBlabelsBjuvenileBjollyB
glisteningBfluidBflirtingBflashesBextraordinaryBexistsBdrainedBdelBcirclingBchoirsBcaBbatmanBaltarBwobbleB
twentyfiveB	treasuresBtootBsmokesBrimB	religiousB
reflectingBprogramBprivacyB
populationBpeekBpassportBoccasionBmummyBmuleBmexicanBlongedBlimpBlifelessBkobeBjerkinBignoringBidealB	hypnotizeBhustlingB	hourglassBhomewardB
hollygroveBguidanceBgleamBfrailBfieryBeventBdododoBcreoleBcozyBbreathesBbreathedBbraceBbieberBbargainB	appealingBagonnaBaffairsBwowompBwhiskyBwardBvaluesBtresBtonsBtaskBstrapsBsmokeyBslugsBslaveryB	seriouslyBrungBrolexBrealmBraisinB	primitiveBpolesBpinballBopinionsBintentBhoverB	freakshowBfoggyBdoomedBdefyBcrippledBcowboysB
couldn’tBcorruptBcoppedBcircumstancesBburdensBbeggarBbearingBbasedBbankerBbachelorBaddingB6xB64BwilderBwiderBwailBtruckinBtoiletBtikiBthereâ€™sBtemptedBtellyBsupernaturalB	sparklingBsalesBrollieBrefereeBreelinBperspectiveBorderedB
melancholyBmarioBmamamamarryBlightedBkoopaB
helicopterBgurlBglowsBf5BexposeBeasterBduringBdepartedBdelayedBcynicalBcowardsB	commotionBbraidsBbowsB
bewilderedBbeliveBaxeBawaitsBatlanticBalertBaaaahB95BwhattupBvirtueBsoleBsnoopyBshacklesBscrapeBscottBrocknB	recordingB	rebellionBquartersBquandoBpleadingBnãoBmakebelieveBlastedBindestructibleBhologramBheadlineBhandyBformingBferalBfendiBehehBdistractionBdeceitBconeB
conclusionBcomplicationsBcologneBcigarBcelebratingBblamedB	basicallyBbachBapplesBzipperB
washingtonBvaletBunconditionalBuhhhBthatâ€™sBsweepingBsunsetsBsteeloBskatingBsignalsBshepherdBreturnsBrelieveBpopeBpoliteBpolicyBpepperB	nevermindBnancyBminimalBmeaninglessBmaneaterBmagnetBlumpsBkittensBkitBkidnapBjanuaryBintenseBiiiiBicingBhugeBharpBgoonBgaryBgapBfriedB
faithfullyBestáBemilyB
downstairsBdominoesBdisappointedBdeerBcurlyBconsolationBcongratulationsBcongoB
collectionBcocoBclearsBchrousB	christinaBcheaperBcarriageBcaravanBbrutalBblegitBblamingBbittenB	believersBbatheBaweBarizonaBamoB41BwattsBwaiterBtrinaBtoraBsuicidalBsublimeBstarveBslowingB	shorteninBshittinBsailinBpuffinBpromBpotsB
motorcycleBmaximumBmangerBlimitedBlegitBlavaBknotsBkingstonBjudgesBjiggleBinfectedBhunnedB	hopefullyBhearseBgroomB	glamorousBgetgetBfreddieBforeplayBfinchBenteredB
disappointBdesBdearestBdcBcowsB	consciousB	communityB
chickadoomBcancelBburnerBbritishBbonfireBblindlyBblendBbatteredBbanditBbandannaBaloudBairsB
ahrououundBahoraByeahhhhhBwritesBuziB	treatmentBthatdB	stressingBsownBslingB	skeletonsBpornoBpolishedBpickupBparoleB	overdriveBnanananaBmiraBlilyBlearnsB	justifiedBjrBjoysBjigBisleBirresistibleBhumansBhankyB	gratitudeBgladnessBgermanBfraudB	evergreenBequalsBdnaBdiddlyBdansBcontinentalBconceiveBcharadeBcdsBcanyonBblastingBbeingsBasapB
armageddonBa5B1984ByearnBweathersBvietnamBtuffB	traditionBtinaBtemptingBtarBswirlingBswampBsurBspikeBslabBshaftBsexingBservantBscrewsBrhythmsBquiBprovedBportraitBpeculiarBorganB	operationBoasisBnapBlickyBlargerBlakersBkiBjoBiiiiiBignorantBhymnBhoodieBhideawayBgroundsBgripsBgingerBgiBfollowinBflossinBfarawayBexileBegyptBedgesBdrizzyB	collectedBcloneBceptBbatchBawfullyBarrivalBangleBvsB
twentyfourB	transformBtougherBtemperaturesBteachingBsurfersBstarringBsposedBsonicBslidinBshorterBsadieBroyBrottingBroosterBrippinBresponsibleBnativeBmuseB	mistakingB
mesmerizedBkeriBkeithBjackedB	instagramBhustlasBhuntedBhankBfiresideBepisodeBendorseBembersBdrenchedBdoorstepB	demandingBdebateB	damnationBcreepyBcontrolsB
controlledBcigarsB	chauffeurBceilingsBbreakerBbrandyBbingBbarrenBavoidingBanticipationB
accustomedB38BwaltzingBvintageBtupeloBtrebleBtrailsBtossingBtormentBstreetlightsBsteepleB
soundtrackB	sidewalksBsenoritaBrefugeBreceivedBraelBqueueBproclaimBprincesB
phenomenonBpaintsBnotherBnoooBmugginB	mentalityB
lalalalalaBkillasBinitialsBilBidleBhymnsB	honeymoonBgroveBgolfBgenocideB	galvestonBfrostyBfroBfreakumBfavourBexplodesB	exclusiveBearthlyBduduBdraftBdavisBd5BcropB
conspiracyBcolouredBclearedBcleanerBclaimedBchoppinBchaserBcementBbuzzinBabyssByouseBwritinBwhitesBvitalBuntoldBunpredictableBtreasonB	tightropeBsyneBsymbolBstoolBsimilarBshortsBrushedBridgeBrarelyB
propagandaB
pittsburghBpintBpayphoneBnosBmorphineBlopezBkindlyBkardinalBirvBinsomniaB	influenceBhotelsBhoseBhopefulBhoodsB	greatnessBgrammyBflyestB
flickeringBfellowsBerrorBentertainingBeazyBdolceBdisappointmentBdevotedBdecentB	dashboardBcourtesyB	companionBclosesBchartBcapsBcansBbondsBbeadsBahahahBactorsBy’allBwreckedBwelfareBveB	undecidedBtwistsBtowBtimboBtacBswayingBsumthinB
subliminalBstylinBspendsBspeedinB	shelteredBshabbyBrailBquakeB	preachingBovertakeBoomBnavyBmoonlitB
mistreatedBmayorBmagneticBlingerieBlebronBlbcBjumpmanBivBinvadeBhashBguardsB
glitteringBfriesB	elegantlyBelasticBeffortBducksB
discussionBdieselBdebrisBdaybreakBcreditsB
commercialBcivilB	charlotteBcasualBcastingBcarveBbleedinBbelovedB	beckoningBbakerBawaitingBawaitBasylumBanothersBanneBaimedBachinBaccentB200Byou’dBwellsBvroomBvidaB	valentinoBusinBuphillBtriggaBteasingBsuffocatingBstinkB	speciallyBsoyBsmellinBslitBslightBskirtsBsickerBshootersBrumorBroyaltyBreaperBradiateBquoteBpromiscuousBpricesBpinnedBoverwhelmingBoutcastBnunBneverendingB	milwaukeeBmhmBmekBmeditateBmaseratiBmaamBlyricBlimboBjuanBjourneysBjohnnysBjermaineBjamunBintoxicatedBinheritBinfoBheatedBgrabbinB	furnitureBfloodsB	explosionBevereverBenteringBduhBdelusionBcubeBclambakeBclaimsBcinnamonBcasinoBbarryBawardBainâ€™tBadlibsBvocalsBtrialsBtokenBtodoBthankingBtaxesBswitchesB	survivingBstompinBseventyBsensualBsambaBprotestBpoemBpmBpeepinBpankyBonewayBnutbushBnogoBnikesB	nashvilleB	misplacedBminiBmercuryBmatBmagicianB	lifesaverBlengthBjawsBjadaBintergalacticB	imaginaryBhutBheeyBguardianBgramB	graduatedBgirliesBfingernailsBfencesBfalterBesteBericBduiB	dragonflyBdpgBdowB	distortedBcurlsBcrooksBcrazeBcontemplatingBclothingBclarityBcelloBbudgetBbienBbackboneBattractBantiBalarmsBaghB187Bx7BwooohBwhistlesBwhatllBwaywardBviolinsBurbanBunaB	thrillingBtastingBslidesBsleetB
skyscraperB	sincerelyB	sexualityBscrapB
resolutionBpunksBpotatoesBpolishBplasterBorbitBojBogsBnightlyBnickelsB
nananananaBnamelessBnaaBmuthafuckasBmountBmerelyBmeltdownBmatesBmaggieBmagBludaBluciferBkeroseneBkaratsBjackpotBhoopBgrievingBgoatBgardensBfortunesB
explainingB	evrybodysBelusiveB
detectivesBdawnsBcountrysideBcounterfeitBcasualtyBcagedBbulldogBbuddhaBbubBbgBavalonBangelinaBak47BadjustBacresBabideBaahhB2010B19B‘tillByangBtvsBtumblinBtrumpetBtroopsB
tonguetiedBthreatsBsystemsBstuffedBstriveBsoothingBsnitchesBsmashingBslumBshhBsharpenBshantyBsessionBrepresentingBrepresentinBprocessBpossessionsB	planetaryBphatB	penetrateBpattyBpapasBnappyBnadaBmedalBmareBmarchinBmansionsBmagooBlickedBlibsBiâ€™veBireneBinstructionsBguddaBgrippinBgrabsBglocksBgayeBfortBformedBflipsBfillinBfeBdemonstrationBcrowsBcrisB
consideredBcoincidenceBcentreBbumpedB	bluebirdsBbeezBbeatlesBawakenedBanarchyBamourBalbumsBaffectBvuittonBtreatinBtastyBsupermarketBstairBstabbedBspitsBspearsB	spaghettiBshowboatBshe´sBsergeantB
rearrangedBrbBrambleBpopoBplottinBpierceBperryBparkinBpadsBoooooooBoctoberBnadieB	mystifiedBmourningBmixingB	loneliestB	limitlessBlennyBkneelingBkisBintimateB	interruptBindiaBhuBhovaBhorizonsBheirBheaterBhappiestBgravelBfloatedBfindinBfckinBfaultsBeveningsBenoughsBdominoBdispositionBdesertsBderangedBcreptBconvictBcarrieB	cardboardBboredomBbonitaBblindfoldedBbeverlyBbasslineBbackedBanglesBammoByawningByatataBweptBwarholBuntillBunlockedBtransformerBtigersBtabooBswearsBsuburbsBsubtleBstrobeBstardustBsprayedBsnowsBskippingBshittyBshaltBscalesBraidBpunchedBprescriptionBpicassoB
peoplepullBoutrunBnapoleonBmaineBlovetronB	landscapeBhumminBhomoBhintBhavanaBhallucinatingBgtrB	freestyleBfortressBfiringBeyelidsBexhaleBequalityBeeyeaBdisrespectfulBdevaB	democracyB
delusionalBcripsB
copacabanaBcontroversyBcontemplateBcodeineBclimbinBchoppedBchokinBceoBcatholicBbreezesBbeggarsBbeaconBbangerBballaBbaeBartistsB	answeringB360B	whirlwindBunlikeB	uninvitedB
unfaithfulBtripperBtravelsBtrappinBterrificBtapesBsweetheartsBsuzieB	surfboardB	squeezingBspatBsomewayBsehBreservedBradiosBpropsBpreachinBpoolsB
politicianB
physicallyBpeersBneighbourhoodBnailedBmásBmustardBmothafuckerBmissusBmeaninBmeanestBlizardBlassieBitalianBinsecuritiesBinfernoBincomprehensibleBimmuneBhighwaysBfacelessBescapingBeraBdemiB	curiosityBcoochieB
complicateBcoconutBcoastingBcluesB	classroomBchoursBcemeteryBcavalryBcabbageBbordersBblanketsB	bewitchedBbenefitBbendsBarthurB	argentinaBallrightB66ByuletideBwitheredBwanaBunoBunforgettableB
underwaterBtingleBtheirsBtengoBsurrealBsuBspreadinBspidersBslowsBslanginBrushesBroastBreservationBrefugeeBredeemBreckonBrabbitsBperformanceB	nightlifeB
mothafuckaBminimumBmillerBmidstBmetroBloungeBkokaneBjockinBinsistBhondaB	greyhoundBgreensBgrailBgeminiBflutterBfebruaryBensembleB	eliminateBdownfallBdaggersBcursesB	crocodileB
convictionB	concealedB
complaininBclearingBclassesBchokedB	chantillyBcellsBcavesBcanadaBbsideBberrybuckmillsstipeBberlinBbathinBarrangeBain´tBagreedBaccuseB	accidentsByoutubeBwhiningBwheatBveniceB
unexpectedBtummyBtereBswamB	struggledBstrikingBsilkenBshoopBsharesBrobbinBreminiscingBrariBpydBonlineBodBnightingaleBnegroBmutualBmuahBmightveBmeowBmeantimeBloveyBlet´sBlensBknuckleBkehaBkatB	jeffersonBitchinBhaircutBgulfBgringoBgongBgiverBgaloreBflawBfilterBfetchBexplanationBdragonsBdevourBderuloBdelilahB
delightfulB	countriesBcoinsBchugalugBchooseyB	chemicalsBchampBceBcavingBcaviarBcaineBbuddiesB	blueprintBblowedBadoredB80B5thB300ByippeeBwopBwondrousBvisaBvillaBvictoriaBvacuumBunnecessaryBtintBtestedBswisherB	suspendedB	strollingBstandardBslowedBslippersBshrinkBshittedBriteBripeBreppingBrepentB	pretendedBplantsBpitifulBpacificBorganicBnicerBnazarethBmuthafckingBmuteBmonroeBmissouriB
millenniumBmerrygoroundBmasteredBmalibuB	lovelightBlockerBlandlordBjacobBharleyBgrandeBgloomyBgaugeBfuckersBfearingB	exquisiteBemiBearthsBduckinBdolphinsBdolphinBdepthBcommunicateBchargesBcatapultBcarryinBcanvasB
boyfriendsBatomBanothaBanalyzeBalarmedBacB90B5xByenByahhhBx1BwwwBwoogieBvillainsBvastB	undressedB	twentyoneBtuxBtigBtickleBtchaB
sympathizeBsuperiorBstitchesBsmotherBshamB	senselessBsafariBrepayBreeferBratedBrallyB	privilegeB	pointlessBphunkBonwardBodysseyBnothinsBnearestBmmhmmBmeasuredBmassiveBmashedBmagikB	lyricallyB	lullabiesBlookieBknockoutBkeshaBkekeBjamminBi’mmaB
interstateB	instinctsBinfantB	impatientBhighlyB
headphonesBhasteBhaitiBguidedBgloriesBfretBfeelinsBexactB	everythinBesteemBentirelyBeasternBcuffsB
corruptionBconstitutionBconfrontationBcoBclumsyBcloudedBchristmasesBcasaBcareyBbumpyB	billboardBbendinBayerB	astronautBxscapeBwilsonBwehBwarninBunbornBtriggersBtribesBswapB	superheroBssBsniffingBskeletonBsisBshrineBshakamBseraBritzBricochetBresponsibilityBrejectedBrearviewBrealisedBpornBpondBphotosBpawBparanoiaBnurseryBmustntBmuffinBmonicaBmmmmmBmagnumBlowlyBlowdownBlovesickBkateBjukeBjigaBjbB	interviewB
interestedBidolBickyBhowlinBhecticBheatsBhatasB
guaranteesBfoxyBfkinBfelonyBeventsBeighthBdoubtfulBdissolveBdangersBcoupesBcosmicB	continuesBconsciousnessBcloutB	caressingBbouquetBboostBbookedBbolderBbipBbedtimeBbeanBauntBarrangedBaplB2pacBziggyBwritersBviolinB
victoriousBupgradeBuneasyBtuningBtitaniumBsuspectB	streamingBsparrowBslugBskippedBskeetBsermonBscheminBscanBripplesBrevolveBquiverB
protectingB	powerlessBpostmanBposseBpatternsBpartyinB	partitionBpaninaroBoficialB
occupationBnovaBnoonesBnoisesBnephewBmurdersBmotiveBmisledBmeltedBmaynBmatrixBmatchingBleoBknucklesBi´llBironicB	infectionBheatherBhandlesBgoooBghostlyBfurtadoBfloodedBflatterB	exhaustedBdissinBdehB	dancehallBcrumbsBcrookBcopperBcommaBclosinBcloakBciciBchucksBcarolB
cannonballBbudgeBblockedBbeotchBbabesBapparentB
apocalypseBantidoteBanimalsmalsBaddsBaddedBwitchesBwindinBwarmsBvousBvenBunwantedBstumpB	strugglesBstockingBstingsBstevenBsteeringB	splittingBspeciesBsoaringBsloppyBslickerBshuttingBshortcutBseasideBscornBsafelyBrowingBrobberyBrinseBrhyminBrenegadeBreflectsBredsB	recommendBreboundBproperlyBprairieBpissinBpatrasB	obliviousBnigB	neglectedBmingleBmessyBlukeBlotionBladenBladdersBkoolBkeepersBjaysBjacketsBirelandBindiansBicebergBhydeBhoveringBheyyyBheartbrakerBhareB	handshakeBhackBfuckaBfrontierBfrictionBfranticBforgetsBfonteB
fascinatedBfangsBeuropeBentwinedB	elephantsBeducatedBearlB
destroyingBdesoBdescendBdepartB	dandelionBcubaBconvertibleB	composureBcoatedBclingsBcherriesBbrosBbossesBbikiniBbedumBattemptBameriB50sBwhensBviolateBvetBvanishedBtruestBtranslationBteflonBteensBtangoBstrumBstickupBsqueezinBsquaresBsnareBsmackedBshredsBshoutinBshakyBsereneBsandalsBroxanneBrookieB	romancingBrobbingB
rhinestoneBqueerBplayyBplatformBpigeonsBperdidoBowlB	overboardB	outskirtsBoprahBoohooBoohhhBonenightBnosesBnormBmammasBlousyBlincolnBlikwitBlexusBleaseBlanaBkaneBjuliaBjaguarBinterventionBinteriorBincB
immaculateBhoundsBhonourBgroanBgrandpaBgrandmasBgenerationsBgangsBfreeholdBfadinBescapedBelegantBeditionBdripsBdramaticBdaydreamingBcurrencyBcurledB	crossfireBcrashinBcornyBconnectionsBchessBchatterBcautiousBcassidyBcanalBbumsBbubbaBbryanBbrewingBbodeBbikesB	baltimoreBballadBatomicBarunBarchBantoneBaggravationB60B32B20sBwuzBwrenchBweavingB	travellerBtintsBthreatenBteaserBstrollinBstrivingB	statementBstagesBspreadsBsoundinBsillBsheerB
sensationsB	selectionBscarfBsandwichBrotationBremovedBreceiverB	realizingBrampageBracistB	professorBpredictableBphantomsBpeepingBoverflowingBouiBoperateBoldenBneverthelessBmp3BmobbinBmmmmmmBmidwestBmenageBmarcyBmadmanBloadsB
limousinesBlambsBlackingBkiloBkiddinBjunkiesBjaggerrichardsB	imperfectBidiotsBiconBhorribleBheatingBgrippingBgassedBflavorsBfireflyBfemalesBendingsBebBeasiestBdrearyBdissatisfiedBdisconnectedBdinosaurB	daydreamsBd7BcyberBchurchesBchicB	characterBceilinBcaptainsBbutcherBbushesBbucketsBbrandedBbowlingBbepBbacardiBapathyBamoreB	alexanderB26B‘roundB
youngbloodBwhoopishBvividBtysonBtrailerBtissueBthomasBtemptationsBtailorBstrippinBstewBsteamingBslipperyBritaB	revolvingBreverieBpurchaseBpillowsBpersistBperonBparkerBoverkillBolayBninjaB	newspaperB
negativityBmickBmensBlosesBlidBlashesBlacesBknightsBjordansBjamieBjamaicanBinvadingB	integrityB
individualB
illuminatiBiggyB
hypnotisedBhostageBhonBhomemadeBhobbyBheraldBheapBhanneninBgroceryBgritBgrinningBgreasedBghettosBgamblinBfemmeB	featuringBenjoyedBdunkBdoveyBdemoBdaredBcreedBcrawlsB	corridorsBconfettiBconceptBcleanedBcherokeeBchequeBc5BbragginBbraffBbonoBboilBbillionsBbennyBbelBbeginninBbankrollBawardsBarkB	anonymousBandreBamusedBalterBalohaB
allegianceBahhhhhBymcmbBwomensBwhassupBwalterBvibrateBtriangleBtransmissionBtraemeloBtornadoBtimothyBthere´sBtatteredBswaggaBstarvinBspookyB	solutionsBsistaB	shalalalaBsatansBsabeBruBrighteousnessBrhymingBrelationBquenchBprevailBpicBpermanentlyBpeeB	partyartyBparaparaparadiseBpagerBpacingBoverwhelmedBoffendedBoblivionBneglectBmindedBmillsBmiddayBmendingB
meditationBmandyBmakersBlikingBlavishBlabBitchingB	inventionBintroducingBintendedBignitionBheavyweightB	heartlandBharkBhandledBgrittyBgrenadeBglossBgeneBfuxBfeminineB	executionBdungareeBdodgingB	disturbedB	disregardBdippedBdillyBcottageBcorpseBcorkBcomicBclashBclanB
christiansB	chevroletBbrrBboughBblouseBamistadBallsB
afflictionBacquaintanceBabsenceByelledByeahyeahyeahBwarrenBwandBwalrusBvioletBversusBtwistaBtsunamiBtrippedBtrillionBtrapsBtracyBtidalB
threatenedBtheoryBsymptomsBswizzBsurfingBsuperficialBstallionBspillinBshuttinB	sheæŠ¯BshaveBsharperBscoldB	scientistBsandyBsagBsadderBrubbleBrobeBrevealsBreupBrattlesnakeBpsyBproudlyB	postcardsBpoppopBpoemsBpicnicBpatsyBoverflowBoffendBneedyBmooreBmoaninBmatildaBlockingBleggedBlearBkennedyB
hurricanesBhortBherdBheckBhazelBgroundedBgollyBforemanBflickersBflatlineBflashedBfittingBfiremanBelementBduperBdresserBdoooBdodododoBdeltaBdazedBculoBcordBcopaBconsumeBconflictBconfinedB
conditionsB	brutalityBbredBblogBbizarreBbittiesBbarbaraBbanjoBarmourBallergicBacesBaaliyahBzerosByungByumsByouuuuByoungnByeeB
wouldn’tBwhopBweighingBwealthyBwagesBvansBvagueBtypesBtrampledBtokBtntB	territoryBtadadadaBswervinBsweatsBswallowsBsupremesBstalkingBstaleB	souvenirsBsnackBslateBsigelBrolesBroadsideBresurrectionBrespectableBresolveBreplacementB	receivingBrankBproducerB	principleBporkBplatterBpenitentiaryBpenisBpedestalBpawsBpasturesBmotivesBmockBmindlessBmañanaBmattaBmarlonBmaidsBmadamBmackinBlungBlllloveB
lieutenantBlayedBkungBjupiterBiæŠ¦BinterestingB	imaginingB
hystericalBhumorBhoppingB	handcuffsBhallwaysBgluedBgiggleBgenerousBfurnaceB	fragranceBflicksBfastestB
evangelineBeclipseBe5BdrownsB
distortionBdilBdetailB
demolitionB	daffodilsBdaddyoB	conceivedBcomedyBcollarsBclappinBchewingBcherB	cathedralBcalendarBbreastsBbraceletBbottledBbompB
blackberryBbelfastBbegsBbankheadBapBanthonyB
ammunitionB94B3dB‘tilBzoominBzoninByou´veByieldB
waterfallsBwannabeBvezBvalerieBtributeBtreadingBtoilBtickinBthurstonBthroughtBtekBteamsBswissB	survivorsBsubeBstellaBstarterBsoftenBsmallestBslotBsinnedBshoutoutBsettlingB
selfesteemBscissorsBsawedBsaberBrequireBrecognitionBpurityBpumpkinBprodigalB	precisionBposedBporterB	policemanBpierBphiladelphiaBpeanutBpainterB	outshinedBnecessarilyBmurkBmockingBmichiganB	mentionedBmegaBluggageBlovegameBloudlyBlocationB	limelightBlesserBleanedBjumpsBjoyousBintelligenceBibizaBhoooBholmesBhiveBguessinBgooooBgonnBgigBframedBfierceBfearedBexerciseBdoodleBdmB	dixielandB
disturbingBdiggerBdeservedBdemonstrateBcuresBcrueltyBcrownedBcontradictionBconsoleBcongregationBcomeaB	collisionBclimaxBchitownBceremonyBcellularBcajunBbranchBboxingB	bodyguardBblurredBblossomsBbleakBbeanieBbassheadBbaggyBarrogantBargumentBaffectedBablazeBa1B69B2012Byouâ€™llByippieBwompBvicBtruthfulBtingsBtecBtanksB	tangerineBsurvivesBsunkenBstunningBstaggerBsqueezedB	splintersBspillsBspiesBsophisticatedBsohoBsnitchinBslammedBskunkBscorpioBsaviorsBrunawaysBrugerBrudyBrobberB	readymadeBradiantBpussiesBpursuitBpredictBpostcardBplyouBoutcomeBnycBnumberedBnbaBmuseumBmossBmoralsBmisleadBmichealBmartyrsBmannieBmadisonBluBliftinBlaxBjoesBignBidealsBhomeboysBheeeheeyBgimmickBg4BfoppBfinesseBfeebleBembarrassedBdownloadBdistrictBdiffrentBdevicesBdereBdebtsBdadadaBcrumblesBcrazierBcoupBcoolestBciBcharliesBcharismaB
cellophaneB
brillianceB
braveheartBbonafideBbloodedBblackedBbessBbasicsB	awakeningBantBamandaBallahBalignedB86ByardsBwitsBwhamBwardrobeBvirginsBuprightB
universityBtumbledBtrumpBtradesBtoiBtemplesBsuwoopB	supernovaBsuburbiaBstunnedBstinkingBsteadBstaffBspliffBslidB	shorelineBshadowyBsequelBseemonBscoresBrustedBrivalBresortBresignedBreinsBramboBramblingBraincoatB	psychoticBpiercedBpermB	performedBpassiveBpaaBoliviaBmoscowB	misbehaveBmillionairesBmigrateBmicsBmarathonBmammyBmafiaBlottoBlinksBlanternBkatyBkarenBjudgingBjazzeBjangleBitchyB	hypocrisyBgrapeBglancesBgirlyBgettaBgeorgieBgazedBgatorsBfundsBfridgeBfranklinBforestsBemceesBemailBeinsteinBeditBdrifterBdrapedBdonaldB	disbeliefBdineroBdeviceBcyanideBcuntBcrackerBcoppersBcombatBchristmastimeBcheyenneBcheaterB
chandelierBcarolsBcaramelBbutterscotchBbutchBbrushedB	bojanglesBblinkingBbessieBberserkBbeautysBbboysB	bandstandBballroomBbabygirlBbabababyBarmiesBarguingBalignB88B´causeBwe’veBwayyBwaterlooBvanzantB	vanishingBtwimbleBtuttiBtrickyBthrottleB	techniqueBstruttinBstirsBsneakinBscheduleBrubbersBricanBreserveBremainedBregalBreflexB
recognizedBprovenBprofitsB	probationB	pressurinBpraysBpeerBparksBoutsB	oohoohoohBniaBnetsBmountainsideBmissionariesBministerB
maybelleneBmaisBmadameBlonerB	literallyBleoneBjoshuaBjackieBintactBhoedownBheyheyBhassleBhandingBgreekBgrandsBfuinBfrankensteinBfilmsBfaggotsBexistedBengineerBemployB	embracingBelevatedBdryingBdrowninBdiningB	defendingBdeeeBdawgsBdaggerBcrutchBcraziestBcoveringBcoppinB	conductorBcolumbusBchoppaB
chickaboomBchainzBcarlosBcakesBcabaretBbutteredBbumpsBbuildsBbolshyBboilingBboardsBbluffinBblogsBbitinBbethBbbangBbannedB	badreallyBbabaBapolloB
alienationB
aftertasteByoungestByawnBwretchedBwipingBwavenBwardenBwallaBvehicleB
underratedBtwitchBtwerkingB	twentiethBtugBtonesBtolerateBthreadsBterminalBtatsB	sunflowerBstudiedBstampsB	staircaseBspeckBsoresB
skateboardBsinksBshopsB
shatteringBshaqBselfcontrolB	secretaryBsaferBryeBrewriteBrevelationsBresideB
relentlessBreignsB	rehearsedBreddiBrecreateBramblerBquitsBpursueB	promisingBprintsBprickBpranceB	poisonousBpleasesBpebbleBozoneB	movementsBmainlineBlingoB
lifestylesBleisureBlatchBlahBladsBjillBjamsBiphoneB	hillbillyBgbB	gardeningBfrownsB	fragmentsBforwardsBforcingBfledB
flashlightBfascinatingBfamilysBfakersBextremesBextendedBequippedBengagedBeconomyBearlierBdinnersBdessertBdesiredB
departmentBdeliveryBdckBdaisiesBcursingBcrippleBcrackersBcouchesB	consumingBcommeBcomebackBcolumbiaBclappingBchewedBcardiacBbuhBbourbonBbiatchBbarbedB	backwoodsBattacksBamazonBacominBaccountsBacceptedB247B
yourselvesByounginBy0uBwooooBwhatevaBweâ€™reBweirdoBweighsB	waistlineBvolcanoB	unlimitedBtroopBtrimB
trampolineBtrainingBtaurusBtauntBsuperstitionB
submissionBstunBstashedBstarkBsplitsBsmokerBskippinBsiameseBservantsBsavagesB
sacrificesBruthlessBroamingB	revealingBrescuedBrelatedBreadinBprudenceBpropositionB	petrifiedBnicestBmissileBmarshallB	marillionB	mannequinBmalcolmBloudestBlashBkneltBitalyBintendB	insuranceB	immortalsBhuggingB	honeycombBharshBhairedBgroovesBgogirlBgalwayBfumesBfoamBflowhahBfingerprintsBfbiBeskimoBelaBdoubtedBdottedBdoaBdivisionBdialingBdeterminationB	detectiveBdefinedBdaresBcsBcontestB	completedBcometBcocktailBcobwebsBcleanseBcitizensBcamB	broadcastBbreakupB	blindfoldBblancheBbidnessBbentleysBbecuzBballetBbackdoorBbabyfaceB
automobileBautoBauntieBantiqueBalphabetBaliensB	airplanesByourreBxxoBwoolBwon´tB
wildflowerBwandererBvultureBvisualBvirusBvileBventBvanessaBunloadB
underworldBuncertaintyBtrendsBtrenchBtravisBtoungeBthymeBthumpinB	throwdownB	throwbackBtheaterBtenthBtedBtaxmanBtartBstrickenBstirringB	standardsBspankBspadeBsomedaysBsnatchedB	snakebiteBshamedBseekerB	scratchesB	saxophoneBsankBringoBretroBrequiemBrenewBrecordedBrattlingBradioactiveBquitterBprosperBprerogativeB
perceptionBouncesBouBoooweeB	obstaclesBnearnessBndBmvpBmemorizeBmelissaBmeeB
mastermindBmarBmaddenBlocketBlestBlamaBkiddBkhakisBkatieBjulianBjahB
hypocritesBhusselBhusbandsBhungoverBhollandBheartbreaksB	headlightBgorillasBgoodeBgonaBglittersBglidingBfreddyBflowedBfiveoBfightersBfagBfabBeyoB
extinctionBexpandBenriqueBemberBelbowsBdrasticBdisorderB	disgustedBdisarrayBdiorBdefyingBdatingBdankBcypressBcuandoB	continuedBcondomBcoalsB	clockworkBcivilizationBbunsBbundleB	braceletsBblastedBbarberBamusingBamateurBallenBaleBairwavesBabsoluteBabroadB92B‘boutByertleBworkerB
windshieldBuntouchableBtrickedBtightenBticksBthunderbirdBtearstainedBswoopB
swallowingBsummonBstonyB
starstruckBsplashinBsmelledBslainBshinningBshawnBseperateBscrewingBrevivalBresponseBreefBrebelsBprehookB
practicingBpiledBphoneyBpaddyBozBoohohohBoinkBnotionsBnooooBnoahBnileBnighBnicksBnaalBmustangBmommysBmeasuresBmcvieBmcqueenBmastBmajorityBlogoBleveeBlaunchBknackBklackBjeremyBjenBjavaBjacuzziBinvestBinvadedB	intellectBimperialBhughBhudsonBhomageBhavocBhaaaaBgritsBglenBg7BfurryBfreshestBflareBferrisBfeeninBfacadeBexcitationsBevryoneBenemysBehhBe7BdublinBdreamyBdivorceBdippingB	dadadootnBcrucialBcreatorB	condemnedB
complimentBcolorfulBclaiminBchicaBchesterBchainsawBcarefreeBbreedsBboomingB	blackeyedBbindsBbackstreetsBasiaB	arroganceBahhaBagingB4sB1xBwaryBveteranBvergeB
vegetablesBvaliumBungluedBunconsciousBtwisterBtweedleBtuaBtrynB
transistorBtoughestBtollsBtippingBthat´sBteenyBsullenB	stupifiedBstocksBstarspangledBspottedBsosoBslobBshootoutBsheenBsheddingB
separationBsandmanBsacksBrulingBrobotsB
regenerateBrebuildBrangerBramonaBpoundinBpooBpiningBpillarsBparasiteB
oppressionBomgBollieBnovelBncBmoetBlowriderBlightenBkamaBjoseBjonahB	injusticeBinfamousBhowardBhohBheroineBhatefulB
halleluyahBfullestBfugitiveBfuegoB	fortyfiveBforfeitBfloydBfleetBfilesBfacinBenviadoBemperorBempathyBdupBdownheartedBdoubtingBdistractions
��
Const_4Const*
_output_shapes	
:�N*
dtype0	*��
value��B��	�N"��                                                 	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �                                                              	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �       	      	      	      	      	      	      	      	      	      		      
	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	       	      !	      "	      #	      $	      %	      &	      '	      (	      )	      *	      +	      ,	      -	      .	      /	      0	      1	      2	      3	      4	      5	      6	      7	      8	      9	      :	      ;	      <	      =	      >	      ?	      @	      A	      B	      C	      D	      E	      F	      G	      H	      I	      J	      K	      L	      M	      N	      O	      P	      Q	      R	      S	      T	      U	      V	      W	      X	      Y	      Z	      [	      \	      ]	      ^	      _	      `	      a	      b	      c	      d	      e	      f	      g	      h	      i	      j	      k	      l	      m	      n	      o	      p	      q	      r	      s	      t	      u	      v	      w	      x	      y	      z	      {	      |	      }	      ~	      	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	       
      
      
      
      
      
      
      
      
      	
      

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
       
      !
      "
      #
      $
      %
      &
      '
      (
      )
      *
      +
      ,
      -
      .
      /
      0
      1
      2
      3
      4
      5
      6
      7
      8
      9
      :
      ;
      <
      =
      >
      ?
      @
      A
      B
      C
      D
      E
      F
      G
      H
      I
      J
      K
      L
      M
      N
      O
      P
      Q
      R
      S
      T
      U
      V
      W
      X
      Y
      Z
      [
      \
      ]
      ^
      _
      `
      a
      b
      c
      d
      e
      f
      g
      h
      i
      j
      k
      l
      m
      n
      o
      p
      q
      r
      s
      t
      u
      v
      w
      x
      y
      z
      {
      |
      }
      ~
      
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                                      	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �        !      !      !      !      !      !      !      !      !      	!      
!      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !       !      !!      "!      #!      $!      %!      &!      '!      (!      )!      *!      +!      ,!      -!      .!      /!      0!      1!      2!      3!      4!      5!      6!      7!      8!      9!      :!      ;!      <!      =!      >!      ?!      @!      A!      B!      C!      D!      E!      F!      G!      H!      I!      J!      K!      L!      M!      N!      O!      P!      Q!      R!      S!      T!      U!      V!      W!      X!      Y!      Z!      [!      \!      ]!      ^!      _!      `!      a!      b!      c!      d!      e!      f!      g!      h!      i!      j!      k!      l!      m!      n!      o!      p!      q!      r!      s!      t!      u!      v!      w!      x!      y!      z!      {!      |!      }!      ~!      !      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!       "      "      "      "      "      "      "      "      "      	"      
"      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "       "      !"      ""      #"      $"      %"      &"      '"      ("      )"      *"      +"      ,"      -"      ."      /"      0"      1"      2"      3"      4"      5"      6"      7"      8"      9"      :"      ;"      <"      ="      >"      ?"      @"      A"      B"      C"      D"      E"      F"      G"      H"      I"      J"      K"      L"      M"      N"      O"      P"      Q"      R"      S"      T"      U"      V"      W"      X"      Y"      Z"      ["      \"      ]"      ^"      _"      `"      a"      b"      c"      d"      e"      f"      g"      h"      i"      j"      k"      l"      m"      n"      o"      p"      q"      r"      s"      t"      u"      v"      w"      x"      y"      z"      {"      |"      }"      ~"      "      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"       #      #      #      #      #      #      #      #      #      	#      
#      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #       #      !#      "#      ##      $#      %#      &#      '#      (#      )#      *#      +#      ,#      -#      .#      /#      0#      1#      2#      3#      4#      5#      6#      7#      8#      9#      :#      ;#      <#      =#      >#      ?#      @#      A#      B#      C#      D#      E#      F#      G#      H#      I#      J#      K#      L#      M#      N#      O#      P#      Q#      R#      S#      T#      U#      V#      W#      X#      Y#      Z#      [#      \#      ]#      ^#      _#      `#      a#      b#      c#      d#      e#      f#      g#      h#      i#      j#      k#      l#      m#      n#      o#      p#      q#      r#      s#      t#      u#      v#      w#      x#      y#      z#      {#      |#      }#      ~#      #      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#       $      $      $      $      $      $      $      $      $      	$      
$      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $       $      !$      "$      #$      $$      %$      &$      '$      ($      )$      *$      +$      ,$      -$      .$      /$      0$      1$      2$      3$      4$      5$      6$      7$      8$      9$      :$      ;$      <$      =$      >$      ?$      @$      A$      B$      C$      D$      E$      F$      G$      H$      I$      J$      K$      L$      M$      N$      O$      P$      Q$      R$      S$      T$      U$      V$      W$      X$      Y$      Z$      [$      \$      ]$      ^$      _$      `$      a$      b$      c$      d$      e$      f$      g$      h$      i$      j$      k$      l$      m$      n$      o$      p$      q$      r$      s$      t$      u$      v$      w$      x$      y$      z$      {$      |$      }$      ~$      $      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$       %      %      %      %      %      %      %      %      %      	%      
%      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %       %      !%      "%      #%      $%      %%      &%      '%      (%      )%      *%      +%      ,%      -%      .%      /%      0%      1%      2%      3%      4%      5%      6%      7%      8%      9%      :%      ;%      <%      =%      >%      ?%      @%      A%      B%      C%      D%      E%      F%      G%      H%      I%      J%      K%      L%      M%      N%      O%      P%      Q%      R%      S%      T%      U%      V%      W%      X%      Y%      Z%      [%      \%      ]%      ^%      _%      `%      a%      b%      c%      d%      e%      f%      g%      h%      i%      j%      k%      l%      m%      n%      o%      p%      q%      r%      s%      t%      u%      v%      w%      x%      y%      z%      {%      |%      }%      ~%      %      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%       &      &      &      &      &      &      &      &      &      	&      
&      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &       &      !&      "&      #&      $&      %&      &&      '&      (&      )&      *&      +&      ,&      -&      .&      /&      0&      1&      2&      3&      4&      5&      6&      7&      8&      9&      :&      ;&      <&      =&      >&      ?&      @&      A&      B&      C&      D&      E&      F&      G&      H&      I&      J&      K&      L&      M&      N&      O&      P&      Q&      R&      S&      T&      U&      V&      W&      X&      Y&      Z&      [&      \&      ]&      ^&      _&      `&      a&      b&      c&      d&      e&      f&      g&      h&      i&      j&      k&      l&      m&      n&      o&      p&      q&      r&      s&      t&      u&      v&      w&      x&      y&      z&      {&      |&      }&      ~&      &      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&       '      '      '      '      '      '      '      '      '      	'      
'      '      '      '      '      '      
�
StatefulPartitionedCallStatefulPartitionedCall
hash_tableConst_3Const_4*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *$
fR
__inference_<lambda>_561914
&
NoOpNoOp^StatefulPartitionedCall
�;
Const_5Const"/device:CPU:0*
_output_shapes
: *
dtype0*�:
value�:B�: B�:
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer-3
layer-4
layer-5
layer-6
layer_with_weights-2
layer-7
		optimizer

	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature

signatures*
$
_lookup_layer
	keras_api* 
�

embeddings
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses*
�

kernel
bias
	variables
trainable_variables
regularization_losses
 	keras_api
!__call__
*"&call_and_return_all_conditional_losses*
�
#	variables
$trainable_variables
%regularization_losses
&	keras_api
'__call__
*(&call_and_return_all_conditional_losses* 
�
)	variables
*trainable_variables
+regularization_losses
,	keras_api
-_random_generator
.__call__
*/&call_and_return_all_conditional_losses* 
�
0	variables
1trainable_variables
2regularization_losses
3	keras_api
4__call__
*5&call_and_return_all_conditional_losses* 
�
6	variables
7trainable_variables
8regularization_losses
9	keras_api
:_random_generator
;__call__
*<&call_and_return_all_conditional_losses* 
�

=kernel
>bias
?	variables
@trainable_variables
Aregularization_losses
B	keras_api
C__call__
*D&call_and_return_all_conditional_losses*
�
Eiter

Fbeta_1

Gbeta_2
	Hdecay
Ilearning_ratem�m�m�=m�>m�v�v�v�=v�>v�*
'
0
1
2
=3
>4*
'
0
1
2
=3
>4*
* 
�
Jnon_trainable_variables

Klayers
Lmetrics
Mlayer_regularization_losses
Nlayer_metrics

	variables
trainable_variables
regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*
* 
* 
* 

Oserving_default* 
9
Pinput_vocabulary
Qlookup_table
R	keras_api* 
* 
hb
VARIABLE_VALUEembedding/embeddings:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUE*

0*

0*
* 
�
Snon_trainable_variables

Tlayers
Umetrics
Vlayer_regularization_losses
Wlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*
* 
* 
]W
VARIABLE_VALUEconv1d/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUEconv1d/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE*

0
1*

0
1*
* 
�
Xnon_trainable_variables

Ylayers
Zmetrics
[layer_regularization_losses
\layer_metrics
	variables
trainable_variables
regularization_losses
!__call__
*"&call_and_return_all_conditional_losses
&""call_and_return_conditional_losses*
* 
* 
* 
* 
* 
�
]non_trainable_variables

^layers
_metrics
`layer_regularization_losses
alayer_metrics
#	variables
$trainable_variables
%regularization_losses
'__call__
*(&call_and_return_all_conditional_losses
&("call_and_return_conditional_losses* 
* 
* 
* 
* 
* 
�
bnon_trainable_variables

clayers
dmetrics
elayer_regularization_losses
flayer_metrics
)	variables
*trainable_variables
+regularization_losses
.__call__
*/&call_and_return_all_conditional_losses
&/"call_and_return_conditional_losses* 
* 
* 
* 
* 
* 
* 
�
gnon_trainable_variables

hlayers
imetrics
jlayer_regularization_losses
klayer_metrics
0	variables
1trainable_variables
2regularization_losses
4__call__
*5&call_and_return_all_conditional_losses
&5"call_and_return_conditional_losses* 
* 
* 
* 
* 
* 
�
lnon_trainable_variables

mlayers
nmetrics
olayer_regularization_losses
player_metrics
6	variables
7trainable_variables
8regularization_losses
;__call__
*<&call_and_return_all_conditional_losses
&<"call_and_return_conditional_losses* 
* 
* 
* 
^X
VARIABLE_VALUEdense_8/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE*
ZT
VARIABLE_VALUEdense_8/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE*

=0
>1*

=0
>1*
* 
�
qnon_trainable_variables

rlayers
smetrics
tlayer_regularization_losses
ulayer_metrics
?	variables
@trainable_variables
Aregularization_losses
C__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses*
* 
* 
LF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE*
PJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE*
PJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE*
NH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
* 
<
0
1
2
3
4
5
6
7*

v0
w1*
* 
* 
* 
* 
R
x_initializer
y_create_resource
z_initialize
{_destroy_resource* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
8
	|total
	}count
~	variables
	keras_api*
M

�total

�count
�
_fn_kwargs
�	variables
�	keras_api*
* 
* 
* 
* 
SM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
SM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*

|0
}1*

~	variables*
UO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE*
UO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE*
* 

�0
�1*

�	variables*
��
VARIABLE_VALUEAdam/embedding/embeddings/mVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�z
VARIABLE_VALUEAdam/conv1d/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
|v
VARIABLE_VALUEAdam/conv1d/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/dense_8/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
}w
VARIABLE_VALUEAdam/dense_8/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
��
VARIABLE_VALUEAdam/embedding/embeddings/vVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�z
VARIABLE_VALUEAdam/conv1d/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
|v
VARIABLE_VALUEAdam/conv1d/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/dense_8/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
}w
VARIABLE_VALUEAdam/dense_8/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
z
serving_default_input_9Placeholder*'
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCall_1StatefulPartitionedCallserving_default_input_9
hash_tableConstConst_1Const_2embedding/embeddingsconv1d/kernelconv1d/biasdense_8/kerneldense_8/bias*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *-
f(R&
$__inference_signature_wrapper_561749
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�	
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filename(embedding/embeddings/Read/ReadVariableOp!conv1d/kernel/Read/ReadVariableOpconv1d/bias/Read/ReadVariableOp"dense_8/kernel/Read/ReadVariableOp dense_8/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp/Adam/embedding/embeddings/m/Read/ReadVariableOp(Adam/conv1d/kernel/m/Read/ReadVariableOp&Adam/conv1d/bias/m/Read/ReadVariableOp)Adam/dense_8/kernel/m/Read/ReadVariableOp'Adam/dense_8/bias/m/Read/ReadVariableOp/Adam/embedding/embeddings/v/Read/ReadVariableOp(Adam/conv1d/kernel/v/Read/ReadVariableOp&Adam/conv1d/bias/v/Read/ReadVariableOp)Adam/dense_8/kernel/v/Read/ReadVariableOp'Adam/dense_8/bias/v/Read/ReadVariableOpConst_5*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *(
f#R!
__inference__traced_save_562016
�
StatefulPartitionedCall_3StatefulPartitionedCallsaver_filenameembedding/embeddingsconv1d/kernelconv1d/biasdense_8/kerneldense_8/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/embedding/embeddings/mAdam/conv1d/kernel/mAdam/conv1d/bias/mAdam/dense_8/kernel/mAdam/dense_8/bias/mAdam/embedding/embeddings/vAdam/conv1d/kernel/vAdam/conv1d/bias/vAdam/dense_8/kernel/vAdam/dense_8/bias/v*$
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__traced_restore_562098��

�~
�
H__inference_sequential_8_layer_call_and_return_conditional_losses_561626

inputsX
Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handleY
Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value	/
+text_vectorization_string_lookup_10_equal_y2
.text_vectorization_string_lookup_10_selectv2_t	4
!embedding_embedding_lookup_561593:	�NdI
2conv1d_conv1d_expanddims_1_readvariableop_resource:d�5
&conv1d_biasadd_readvariableop_resource:	�:
&dense_8_matmul_readvariableop_resource:
��6
'dense_8_biasadd_readvariableop_resource:	�
identity��conv1d/BiasAdd/ReadVariableOp�)conv1d/Conv1D/ExpandDims_1/ReadVariableOp�dense_8/BiasAdd/ReadVariableOp�dense_8/MatMul/ReadVariableOp�embedding/embedding_lookup�Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2^
text_vectorization/StringLowerStringLowerinputs*'
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2#text_vectorization/Squeeze:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
)text_vectorization/string_lookup_10/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0+text_vectorization_string_lookup_10_equal_y*
T0*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/SelectV2SelectV2-text_vectorization/string_lookup_10/Equal:z:0.text_vectorization_string_lookup_10_selectv2_tPtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/IdentityIdentity5text_vectorization/string_lookup_10/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:05text_vectorization/string_lookup_10/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_561593?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tindices0	*4
_class*
(&loc:@embedding/embedding_lookup/561593*,
_output_shapes
:����������d*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/561593*,
_output_shapes
:����������d�
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dg
conv1d/Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
conv1d/Conv1D/ExpandDims
ExpandDims.embedding/embedding_lookup/Identity_1:output:0%conv1d/Conv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
)conv1d/Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp2conv1d_conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0`
conv1d/Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
conv1d/Conv1D/ExpandDims_1
ExpandDims1conv1d/Conv1D/ExpandDims_1/ReadVariableOp:value:0'conv1d/Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
conv1d/Conv1DConv2D!conv1d/Conv1D/ExpandDims:output:0#conv1d/Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
conv1d/Conv1D/SqueezeSqueezeconv1d/Conv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

����������
conv1d/BiasAdd/ReadVariableOpReadVariableOp&conv1d_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv1d/BiasAddBiasAddconv1d/Conv1D/Squeeze:output:0%conv1d/BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������d
conv1d/ReluReluconv1d/BiasAdd:output:0*
T0*-
_output_shapes
:�����������^
max_pooling1d/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :�
max_pooling1d/ExpandDims
ExpandDimsconv1d/Relu:activations:0%max_pooling1d/ExpandDims/dim:output:0*
T0*1
_output_shapes
:������������
max_pooling1d/MaxPoolMaxPool!max_pooling1d/ExpandDims:output:0*0
_output_shapes
:���������b�*
ksize
*
paddingVALID*
strides
�
max_pooling1d/SqueezeSqueezemax_pooling1d/MaxPool:output:0*
T0*,
_output_shapes
:���������b�*
squeeze_dims
u
dropout_1/IdentityIdentitymax_pooling1d/Squeeze:output:0*
T0*,
_output_shapes
:���������b�l
*global_max_pooling1d/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_max_pooling1d/MaxMaxdropout_1/Identity:output:03global_max_pooling1d/Max/reduction_indices:output:0*
T0*(
_output_shapes
:����������t
dropout_2/IdentityIdentity!global_max_pooling1d/Max:output:0*
T0*(
_output_shapes
:�����������
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_8/MatMulMatMuldropout_2/Identity:output:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:�����������
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������g
dense_8/SoftmaxSoftmaxdense_8/BiasAdd:output:0*
T0*(
_output_shapes
:����������i
IdentityIdentitydense_8/Softmax:softmax:0^NoOp*
T0*(
_output_shapes
:�����������
NoOpNoOp^conv1d/BiasAdd/ReadVariableOp*^conv1d/Conv1D/ExpandDims_1/ReadVariableOp^dense_8/BiasAdd/ReadVariableOp^dense_8/MatMul/ReadVariableOp^embedding/embedding_lookupH^text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2>
conv1d/BiasAdd/ReadVariableOpconv1d/BiasAdd/ReadVariableOp2V
)conv1d/Conv1D/ExpandDims_1/ReadVariableOp)conv1d/Conv1D/ExpandDims_1/ReadVariableOp2@
dense_8/BiasAdd/ReadVariableOpdense_8/BiasAdd/ReadVariableOp2>
dense_8/MatMul/ReadVariableOpdense_8/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup2�
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
;
__inference__creator_561893
identity��
hash_tablen

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name494895*
value_dtype0	W
IdentityIdentityhash_table:table_handle:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp^hash_table*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2

hash_table
hash_table
�
F
*__inference_dropout_2_layer_call_fn_561846

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_2_layer_call_and_return_conditional_losses_561099a
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
__inference__initializer_5619019
5key_value_init494894_lookuptableimportv2_table_handle1
-key_value_init494894_lookuptableimportv2_keys3
/key_value_init494894_lookuptableimportv2_values	
identity��(key_value_init494894/LookupTableImportV2�
(key_value_init494894/LookupTableImportV2LookupTableImportV25key_value_init494894_lookuptableimportv2_table_handle-key_value_init494894_lookuptableimportv2_keys/key_value_init494894_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 G
ConstConst*
_output_shapes
: *
dtype0*
value	B :L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: q
NoOpNoOp)^key_value_init494894/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
: :�N:�N2T
(key_value_init494894/LookupTableImportV2(key_value_init494894/LookupTableImportV2:!

_output_shapes	
:�N:!

_output_shapes	
:�N
�7
�	
__inference__traced_save_562016
file_prefix3
/savev2_embedding_embeddings_read_readvariableop,
(savev2_conv1d_kernel_read_readvariableop*
&savev2_conv1d_bias_read_readvariableop-
)savev2_dense_8_kernel_read_readvariableop+
'savev2_dense_8_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop:
6savev2_adam_embedding_embeddings_m_read_readvariableop3
/savev2_adam_conv1d_kernel_m_read_readvariableop1
-savev2_adam_conv1d_bias_m_read_readvariableop4
0savev2_adam_dense_8_kernel_m_read_readvariableop2
.savev2_adam_dense_8_bias_m_read_readvariableop:
6savev2_adam_embedding_embeddings_v_read_readvariableop3
/savev2_adam_conv1d_kernel_v_read_readvariableop1
-savev2_adam_conv1d_bias_v_read_readvariableop4
0savev2_adam_dense_8_kernel_v_read_readvariableop2
.savev2_adam_dense_8_bias_v_read_readvariableop
savev2_const_5

identity_1��MergeV2Checkpointsw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: �
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �	
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0/savev2_embedding_embeddings_read_readvariableop(savev2_conv1d_kernel_read_readvariableop&savev2_conv1d_bias_read_readvariableop)savev2_dense_8_kernel_read_readvariableop'savev2_dense_8_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop6savev2_adam_embedding_embeddings_m_read_readvariableop/savev2_adam_conv1d_kernel_m_read_readvariableop-savev2_adam_conv1d_bias_m_read_readvariableop0savev2_adam_dense_8_kernel_m_read_readvariableop.savev2_adam_dense_8_bias_m_read_readvariableop6savev2_adam_embedding_embeddings_v_read_readvariableop/savev2_adam_conv1d_kernel_v_read_readvariableop-savev2_adam_conv1d_bias_v_read_readvariableop0savev2_adam_dense_8_kernel_v_read_readvariableop.savev2_adam_dense_8_bias_v_read_readvariableopsavev2_const_5"/device:CPU:0*
_output_shapes
 *'
dtypes
2	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 f
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: Q

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: [
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*�
_input_shapes�
�: :	�Nd:d�:�:
��:�: : : : : : : : : :	�Nd:d�:�:
��:�:	�Nd:d�:�:
��:�: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:%!

_output_shapes
:	�Nd:)%
#
_output_shapes
:d�:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :%!

_output_shapes
:	�Nd:)%
#
_output_shapes
:d�:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�Nd:)%
#
_output_shapes
:d�:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:

_output_shapes
: 
�	
�
$__inference_signature_wrapper_561749
input_9
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:
��
	unknown_7:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_9unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� **
f%R#
!__inference__wrapped_model_560967p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:���������
!
_user_specified_name	input_9:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
e
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_560979

inputs
identityP
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :�

ExpandDims
ExpandDimsinputsExpandDims/dim:output:0*
T0*A
_output_shapes/
-:+����������������������������
MaxPoolMaxPoolExpandDims:output:0*A
_output_shapes/
-:+���������������������������*
ksize
*
paddingVALID*
strides
�
SqueezeSqueezeMaxPool:output:0*
T0*=
_output_shapes+
):'���������������������������*
squeeze_dims
n
IdentityIdentitySqueeze:output:0*
T0*=
_output_shapes+
):'���������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�
�
E__inference_embedding_layer_call_and_return_conditional_losses_561765

inputs	*
embedding_lookup_561759:	�Nd
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_561759inputs*
Tindices0	**
_class 
loc:@embedding_lookup/561759*,
_output_shapes
:����������d*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0**
_class 
loc:@embedding_lookup/561759*,
_output_shapes
:����������d�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dx
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*,
_output_shapes
:����������dY
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 2$
embedding_lookupembedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�	
d
E__inference_dropout_2_layer_call_and_return_conditional_losses_561868

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @e
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������p
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������j
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������Z
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
c
*__inference_dropout_2_layer_call_fn_561851

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_2_layer_call_and_return_conditional_losses_561170p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
c
E__inference_dropout_2_layer_call_and_return_conditional_losses_561856

inputs

identity_1O
IdentityIdentityinputs*
T0*(
_output_shapes
:����������\

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�q
�
H__inference_sequential_8_layer_call_and_return_conditional_losses_561119

inputsX
Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handleY
Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value	/
+text_vectorization_string_lookup_10_equal_y2
.text_vectorization_string_lookup_10_selectv2_t	#
embedding_561060:	�Nd$
conv1d_561080:d�
conv1d_561082:	�"
dense_8_561113:
��
dense_8_561115:	�
identity��conv1d/StatefulPartitionedCall�dense_8/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2^
text_vectorization/StringLowerStringLowerinputs*'
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2#text_vectorization/Squeeze:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
)text_vectorization/string_lookup_10/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0+text_vectorization_string_lookup_10_equal_y*
T0*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/SelectV2SelectV2-text_vectorization/string_lookup_10/Equal:z:0.text_vectorization_string_lookup_10_selectv2_tPtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/IdentityIdentity5text_vectorization/string_lookup_10/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:05text_vectorization/string_lookup_10/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
!embedding/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0embedding_561060*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_561059�
conv1d/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0conv1d_561080conv1d_561082*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_conv1d_layer_call_and_return_conditional_losses_561079�
max_pooling1d/PartitionedCallPartitionedCall'conv1d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *R
fMRK
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_560979�
dropout_1/PartitionedCallPartitionedCall&max_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_561091�
$global_max_pooling1d/PartitionedCallPartitionedCall"dropout_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_560992�
dropout_2/PartitionedCallPartitionedCall-global_max_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_2_layer_call_and_return_conditional_losses_561099�
dense_8/StatefulPartitionedCallStatefulPartitionedCall"dropout_2/PartitionedCall:output:0dense_8_561113dense_8_561115*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_561112x
IdentityIdentity(dense_8/StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:�����������
NoOpNoOp^conv1d/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall"^embedding/StatefulPartitionedCallH^text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2@
conv1d/StatefulPartitionedCallconv1d/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2�
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
F
*__inference_dropout_1_layer_call_fn_561808

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_561091e
IdentityIdentityPartitionedCall:output:0*
T0*,
_output_shapes
:���������b�"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:���������b�:T P
,
_output_shapes
:���������b�
 
_user_specified_nameinputs
�a
�
"__inference__traced_restore_562098
file_prefix8
%assignvariableop_embedding_embeddings:	�Nd7
 assignvariableop_1_conv1d_kernel:d�-
assignvariableop_2_conv1d_bias:	�5
!assignvariableop_3_dense_8_kernel:
��.
assignvariableop_4_dense_8_bias:	�&
assignvariableop_5_adam_iter:	 (
assignvariableop_6_adam_beta_1: (
assignvariableop_7_adam_beta_2: '
assignvariableop_8_adam_decay: /
%assignvariableop_9_adam_learning_rate: #
assignvariableop_10_total: #
assignvariableop_11_count: %
assignvariableop_12_total_1: %
assignvariableop_13_count_1: B
/assignvariableop_14_adam_embedding_embeddings_m:	�Nd?
(assignvariableop_15_adam_conv1d_kernel_m:d�5
&assignvariableop_16_adam_conv1d_bias_m:	�=
)assignvariableop_17_adam_dense_8_kernel_m:
��6
'assignvariableop_18_adam_dense_8_bias_m:	�B
/assignvariableop_19_adam_embedding_embeddings_v:	�Nd?
(assignvariableop_20_adam_conv1d_kernel_v:d�5
&assignvariableop_21_adam_conv1d_bias_v:	�=
)assignvariableop_22_adam_dense_8_kernel_v:
��6
'assignvariableop_23_adam_dense_8_bias_v:	�
identity_25��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*x
_output_shapesf
d:::::::::::::::::::::::::*'
dtypes
2	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOp%assignvariableop_embedding_embeddingsIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOp assignvariableop_1_conv1d_kernelIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOpassignvariableop_2_conv1d_biasIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOp!assignvariableop_3_dense_8_kernelIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOpassignvariableop_4_dense_8_biasIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_iterIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_beta_1Identity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOpassignvariableop_7_adam_beta_2Identity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOpassignvariableop_8_adam_decayIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOp%assignvariableop_9_adam_learning_rateIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOpassignvariableop_10_totalIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOpassignvariableop_11_countIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOpassignvariableop_12_total_1Identity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOpassignvariableop_13_count_1Identity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOp/assignvariableop_14_adam_embedding_embeddings_mIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOp(assignvariableop_15_adam_conv1d_kernel_mIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp&assignvariableop_16_adam_conv1d_bias_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp)assignvariableop_17_adam_dense_8_kernel_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp'assignvariableop_18_adam_dense_8_bias_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp/assignvariableop_19_adam_embedding_embeddings_vIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp(assignvariableop_20_adam_conv1d_kernel_vIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp&assignvariableop_21_adam_conv1d_bias_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_22AssignVariableOp)assignvariableop_22_adam_dense_8_kernel_vIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_23AssignVariableOp'assignvariableop_23_adam_dense_8_bias_vIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype01
NoOpNoOp"/device:CPU:0*
_output_shapes
 �
Identity_24Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_25IdentityIdentity_24:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 "#
identity_25Identity_25:output:0*E
_input_shapes4
2: : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�

*__inference_embedding_layer_call_fn_561756

inputs	
unknown:	�Nd
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_561059t
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*,
_output_shapes
:����������d`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

d
E__inference_dropout_1_layer_call_and_return_conditional_losses_561193

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @i
dropout/MulMulinputsdropout/Const:output:0*
T0*,
_output_shapes
:���������b�C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*,
_output_shapes
:���������b�*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:���������b�t
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:���������b�n
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*,
_output_shapes
:���������b�^
IdentityIdentitydropout/Mul_1:z:0*
T0*,
_output_shapes
:���������b�"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:���������b�:T P
,
_output_shapes
:���������b�
 
_user_specified_nameinputs
�t
�
H__inference_sequential_8_layer_call_and_return_conditional_losses_561490
input_9X
Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handleY
Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value	/
+text_vectorization_string_lookup_10_equal_y2
.text_vectorization_string_lookup_10_selectv2_t	#
embedding_561472:	�Nd$
conv1d_561475:d�
conv1d_561477:	�"
dense_8_561484:
��
dense_8_561486:	�
identity��conv1d/StatefulPartitionedCall�dense_8/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!dropout_2/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2_
text_vectorization/StringLowerStringLowerinput_9*'
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2#text_vectorization/Squeeze:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
)text_vectorization/string_lookup_10/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0+text_vectorization_string_lookup_10_equal_y*
T0*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/SelectV2SelectV2-text_vectorization/string_lookup_10/Equal:z:0.text_vectorization_string_lookup_10_selectv2_tPtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/IdentityIdentity5text_vectorization/string_lookup_10/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:05text_vectorization/string_lookup_10/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
!embedding/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0embedding_561472*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_561059�
conv1d/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0conv1d_561475conv1d_561477*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_conv1d_layer_call_and_return_conditional_losses_561079�
max_pooling1d/PartitionedCallPartitionedCall'conv1d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *R
fMRK
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_560979�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall&max_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_561193�
$global_max_pooling1d/PartitionedCallPartitionedCall*dropout_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_560992�
!dropout_2/StatefulPartitionedCallStatefulPartitionedCall-global_max_pooling1d/PartitionedCall:output:0"^dropout_1/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_2_layer_call_and_return_conditional_losses_561170�
dense_8/StatefulPartitionedCallStatefulPartitionedCall*dropout_2/StatefulPartitionedCall:output:0dense_8_561484dense_8_561486*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_561112x
IdentityIdentity(dense_8/StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:�����������
NoOpNoOp^conv1d/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^dropout_2/StatefulPartitionedCall"^embedding/StatefulPartitionedCallH^text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2@
conv1d/StatefulPartitionedCallconv1d/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!dropout_2/StatefulPartitionedCall!dropout_2/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2�
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:P L
'
_output_shapes
:���������
!
_user_specified_name	input_9:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
c
E__inference_dropout_1_layer_call_and_return_conditional_losses_561091

inputs

identity_1S
IdentityIdentityinputs*
T0*,
_output_shapes
:���������b�`

Identity_1IdentityIdentity:output:0*
T0*,
_output_shapes
:���������b�"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:���������b�:T P
,
_output_shapes
:���������b�
 
_user_specified_nameinputs
�

�
-__inference_sequential_8_layer_call_fn_561519

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:
��
	unknown_7:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_sequential_8_layer_call_and_return_conditional_losses_561119p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
'__inference_conv1d_layer_call_fn_561774

inputs
unknown:d�
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_conv1d_layer_call_and_return_conditional_losses_561079u
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*-
_output_shapes
:�����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������d: : 22
StatefulPartitionedCallStatefulPartitionedCall:T P
,
_output_shapes
:����������d
 
_user_specified_nameinputs
�
Q
5__inference_global_max_pooling1d_layer_call_fn_561835

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_560992i
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�
c
E__inference_dropout_2_layer_call_and_return_conditional_losses_561099

inputs

identity_1O
IdentityIdentityinputs*
T0*(
_output_shapes
:����������\

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
c
E__inference_dropout_1_layer_call_and_return_conditional_losses_561818

inputs

identity_1S
IdentityIdentityinputs*
T0*,
_output_shapes
:���������b�`

Identity_1IdentityIdentity:output:0*
T0*,
_output_shapes
:���������b�"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:���������b�:T P
,
_output_shapes
:���������b�
 
_user_specified_nameinputs
�
�
(__inference_dense_8_layer_call_fn_561877

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_561112p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
B__inference_conv1d_layer_call_and_return_conditional_losses_561079

inputsB
+conv1d_expanddims_1_readvariableop_resource:d�.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�"Conv1D/ExpandDims_1/ReadVariableOp`
Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
Conv1D/ExpandDims
ExpandDimsinputsConv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
"Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp+conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0Y
Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
Conv1D/ExpandDims_1
ExpandDims*Conv1D/ExpandDims_1/ReadVariableOp:value:0 Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
Conv1DConv2DConv1D/ExpandDims:output:0Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
Conv1D/SqueezeSqueezeConv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

���������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
BiasAddBiasAddConv1D/Squeeze:output:0BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������V
ReluReluBiasAdd:output:0*
T0*-
_output_shapes
:�����������g
IdentityIdentityRelu:activations:0^NoOp*
T0*-
_output_shapes
:������������
NoOpNoOp^BiasAdd/ReadVariableOp#^Conv1D/ExpandDims_1/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������d: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2H
"Conv1D/ExpandDims_1/ReadVariableOp"Conv1D/ExpandDims_1/ReadVariableOp:T P
,
_output_shapes
:����������d
 
_user_specified_nameinputs
�t
�
H__inference_sequential_8_layer_call_and_return_conditional_losses_561308

inputsX
Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handleY
Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value	/
+text_vectorization_string_lookup_10_equal_y2
.text_vectorization_string_lookup_10_selectv2_t	#
embedding_561290:	�Nd$
conv1d_561293:d�
conv1d_561295:	�"
dense_8_561302:
��
dense_8_561304:	�
identity��conv1d/StatefulPartitionedCall�dense_8/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!dropout_2/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2^
text_vectorization/StringLowerStringLowerinputs*'
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2#text_vectorization/Squeeze:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
)text_vectorization/string_lookup_10/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0+text_vectorization_string_lookup_10_equal_y*
T0*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/SelectV2SelectV2-text_vectorization/string_lookup_10/Equal:z:0.text_vectorization_string_lookup_10_selectv2_tPtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/IdentityIdentity5text_vectorization/string_lookup_10/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:05text_vectorization/string_lookup_10/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
!embedding/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0embedding_561290*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_561059�
conv1d/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0conv1d_561293conv1d_561295*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_conv1d_layer_call_and_return_conditional_losses_561079�
max_pooling1d/PartitionedCallPartitionedCall'conv1d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *R
fMRK
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_560979�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall&max_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_561193�
$global_max_pooling1d/PartitionedCallPartitionedCall*dropout_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_560992�
!dropout_2/StatefulPartitionedCallStatefulPartitionedCall-global_max_pooling1d/PartitionedCall:output:0"^dropout_1/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_2_layer_call_and_return_conditional_losses_561170�
dense_8/StatefulPartitionedCallStatefulPartitionedCall*dropout_2/StatefulPartitionedCall:output:0dense_8_561302dense_8_561304*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_561112x
IdentityIdentity(dense_8/StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:�����������
NoOpNoOp^conv1d/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^dropout_2/StatefulPartitionedCall"^embedding/StatefulPartitionedCallH^text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2@
conv1d/StatefulPartitionedCallconv1d/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!dropout_2/StatefulPartitionedCall!dropout_2/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2�
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
l
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_561841

inputs
identityW
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :m
MaxMaxinputsMax/reduction_indices:output:0*
T0*0
_output_shapes
:������������������]
IdentityIdentityMax:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�

d
E__inference_dropout_1_layer_call_and_return_conditional_losses_561830

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @i
dropout/MulMulinputsdropout/Const:output:0*
T0*,
_output_shapes
:���������b�C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*,
_output_shapes
:���������b�*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:���������b�t
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:���������b�n
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*,
_output_shapes
:���������b�^
IdentityIdentitydropout/Mul_1:z:0*
T0*,
_output_shapes
:���������b�"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:���������b�:T P
,
_output_shapes
:���������b�
 
_user_specified_nameinputs
�
l
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_560992

inputs
identityW
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :m
MaxMaxinputsMax/reduction_indices:output:0*
T0*0
_output_shapes
:������������������]
IdentityIdentityMax:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�
�
!__inference__wrapped_model_560967
input_9e
asequential_8_text_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handlef
bsequential_8_text_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value	<
8sequential_8_text_vectorization_string_lookup_10_equal_y?
;sequential_8_text_vectorization_string_lookup_10_selectv2_t	A
.sequential_8_embedding_embedding_lookup_560934:	�NdV
?sequential_8_conv1d_conv1d_expanddims_1_readvariableop_resource:d�B
3sequential_8_conv1d_biasadd_readvariableop_resource:	�G
3sequential_8_dense_8_matmul_readvariableop_resource:
��C
4sequential_8_dense_8_biasadd_readvariableop_resource:	�
identity��*sequential_8/conv1d/BiasAdd/ReadVariableOp�6sequential_8/conv1d/Conv1D/ExpandDims_1/ReadVariableOp�+sequential_8/dense_8/BiasAdd/ReadVariableOp�*sequential_8/dense_8/MatMul/ReadVariableOp�'sequential_8/embedding/embedding_lookup�Tsequential_8/text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2l
+sequential_8/text_vectorization/StringLowerStringLowerinput_9*'
_output_shapes
:����������
2sequential_8/text_vectorization/StaticRegexReplaceStaticRegexReplace4sequential_8/text_vectorization/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
'sequential_8/text_vectorization/SqueezeSqueeze;sequential_8/text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������r
1sequential_8/text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
9sequential_8/text_vectorization/StringSplit/StringSplitV2StringSplitV20sequential_8/text_vectorization/Squeeze:output:0:sequential_8/text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
?sequential_8/text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
Asequential_8/text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
Asequential_8/text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
9sequential_8/text_vectorization/StringSplit/strided_sliceStridedSliceCsequential_8/text_vectorization/StringSplit/StringSplitV2:indices:0Hsequential_8/text_vectorization/StringSplit/strided_slice/stack:output:0Jsequential_8/text_vectorization/StringSplit/strided_slice/stack_1:output:0Jsequential_8/text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
Asequential_8/text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Csequential_8/text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Csequential_8/text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
;sequential_8/text_vectorization/StringSplit/strided_slice_1StridedSliceAsequential_8/text_vectorization/StringSplit/StringSplitV2:shape:0Jsequential_8/text_vectorization/StringSplit/strided_slice_1/stack:output:0Lsequential_8/text_vectorization/StringSplit/strided_slice_1/stack_1:output:0Lsequential_8/text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
bsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCastBsequential_8/text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
dsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1CastDsequential_8/text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
lsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapefsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
lsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
ksequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdusequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0usequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
psequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
nsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatertsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ysequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
ksequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastrsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
nsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
jsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxfsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0wsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
lsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
jsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ssequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0usequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
jsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulosequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0nsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
nsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximumhsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0nsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
nsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimumhsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0rsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
nsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
osequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountfsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0rsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0wsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
isequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
dsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumvsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0rsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
msequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
isequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
dsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2vsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0jsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0rsequential_8/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Tsequential_8/text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2LookupTableFindV2asequential_8_text_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handleBsequential_8/text_vectorization/StringSplit/StringSplitV2:values:0bsequential_8_text_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
6sequential_8/text_vectorization/string_lookup_10/EqualEqualBsequential_8/text_vectorization/StringSplit/StringSplitV2:values:08sequential_8_text_vectorization_string_lookup_10_equal_y*
T0*#
_output_shapes
:����������
9sequential_8/text_vectorization/string_lookup_10/SelectV2SelectV2:sequential_8/text_vectorization/string_lookup_10/Equal:z:0;sequential_8_text_vectorization_string_lookup_10_selectv2_t]sequential_8/text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
9sequential_8/text_vectorization/string_lookup_10/IdentityIdentityBsequential_8/text_vectorization/string_lookup_10/SelectV2:output:0*
T0	*#
_output_shapes
:���������~
<sequential_8/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
4sequential_8/text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
Csequential_8/text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor=sequential_8/text_vectorization/RaggedToTensor/Const:output:0Bsequential_8/text_vectorization/string_lookup_10/Identity:output:0Esequential_8/text_vectorization/RaggedToTensor/default_value:output:0Dsequential_8/text_vectorization/StringSplit/strided_slice_1:output:0Bsequential_8/text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
'sequential_8/embedding/embedding_lookupResourceGather.sequential_8_embedding_embedding_lookup_560934Lsequential_8/text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tindices0	*A
_class7
53loc:@sequential_8/embedding/embedding_lookup/560934*,
_output_shapes
:����������d*
dtype0�
0sequential_8/embedding/embedding_lookup/IdentityIdentity0sequential_8/embedding/embedding_lookup:output:0*
T0*A
_class7
53loc:@sequential_8/embedding/embedding_lookup/560934*,
_output_shapes
:����������d�
2sequential_8/embedding/embedding_lookup/Identity_1Identity9sequential_8/embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dt
)sequential_8/conv1d/Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
%sequential_8/conv1d/Conv1D/ExpandDims
ExpandDims;sequential_8/embedding/embedding_lookup/Identity_1:output:02sequential_8/conv1d/Conv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
6sequential_8/conv1d/Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp?sequential_8_conv1d_conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0m
+sequential_8/conv1d/Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
'sequential_8/conv1d/Conv1D/ExpandDims_1
ExpandDims>sequential_8/conv1d/Conv1D/ExpandDims_1/ReadVariableOp:value:04sequential_8/conv1d/Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
sequential_8/conv1d/Conv1DConv2D.sequential_8/conv1d/Conv1D/ExpandDims:output:00sequential_8/conv1d/Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
"sequential_8/conv1d/Conv1D/SqueezeSqueeze#sequential_8/conv1d/Conv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

����������
*sequential_8/conv1d/BiasAdd/ReadVariableOpReadVariableOp3sequential_8_conv1d_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
sequential_8/conv1d/BiasAddBiasAdd+sequential_8/conv1d/Conv1D/Squeeze:output:02sequential_8/conv1d/BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������~
sequential_8/conv1d/ReluRelu$sequential_8/conv1d/BiasAdd:output:0*
T0*-
_output_shapes
:�����������k
)sequential_8/max_pooling1d/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :�
%sequential_8/max_pooling1d/ExpandDims
ExpandDims&sequential_8/conv1d/Relu:activations:02sequential_8/max_pooling1d/ExpandDims/dim:output:0*
T0*1
_output_shapes
:������������
"sequential_8/max_pooling1d/MaxPoolMaxPool.sequential_8/max_pooling1d/ExpandDims:output:0*0
_output_shapes
:���������b�*
ksize
*
paddingVALID*
strides
�
"sequential_8/max_pooling1d/SqueezeSqueeze+sequential_8/max_pooling1d/MaxPool:output:0*
T0*,
_output_shapes
:���������b�*
squeeze_dims
�
sequential_8/dropout_1/IdentityIdentity+sequential_8/max_pooling1d/Squeeze:output:0*
T0*,
_output_shapes
:���������b�y
7sequential_8/global_max_pooling1d/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
%sequential_8/global_max_pooling1d/MaxMax(sequential_8/dropout_1/Identity:output:0@sequential_8/global_max_pooling1d/Max/reduction_indices:output:0*
T0*(
_output_shapes
:�����������
sequential_8/dropout_2/IdentityIdentity.sequential_8/global_max_pooling1d/Max:output:0*
T0*(
_output_shapes
:�����������
*sequential_8/dense_8/MatMul/ReadVariableOpReadVariableOp3sequential_8_dense_8_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
sequential_8/dense_8/MatMulMatMul(sequential_8/dropout_2/Identity:output:02sequential_8/dense_8/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:�����������
+sequential_8/dense_8/BiasAdd/ReadVariableOpReadVariableOp4sequential_8_dense_8_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
sequential_8/dense_8/BiasAddBiasAdd%sequential_8/dense_8/MatMul:product:03sequential_8/dense_8/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:�����������
sequential_8/dense_8/SoftmaxSoftmax%sequential_8/dense_8/BiasAdd:output:0*
T0*(
_output_shapes
:����������v
IdentityIdentity&sequential_8/dense_8/Softmax:softmax:0^NoOp*
T0*(
_output_shapes
:�����������
NoOpNoOp+^sequential_8/conv1d/BiasAdd/ReadVariableOp7^sequential_8/conv1d/Conv1D/ExpandDims_1/ReadVariableOp,^sequential_8/dense_8/BiasAdd/ReadVariableOp+^sequential_8/dense_8/MatMul/ReadVariableOp(^sequential_8/embedding/embedding_lookupU^sequential_8/text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2X
*sequential_8/conv1d/BiasAdd/ReadVariableOp*sequential_8/conv1d/BiasAdd/ReadVariableOp2p
6sequential_8/conv1d/Conv1D/ExpandDims_1/ReadVariableOp6sequential_8/conv1d/Conv1D/ExpandDims_1/ReadVariableOp2Z
+sequential_8/dense_8/BiasAdd/ReadVariableOp+sequential_8/dense_8/BiasAdd/ReadVariableOp2X
*sequential_8/dense_8/MatMul/ReadVariableOp*sequential_8/dense_8/MatMul/ReadVariableOp2R
'sequential_8/embedding/embedding_lookup'sequential_8/embedding/embedding_lookup2�
Tsequential_8/text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2Tsequential_8/text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:P L
'
_output_shapes
:���������
!
_user_specified_name	input_9:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�

�
-__inference_sequential_8_layer_call_fn_561352
input_9
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:
��
	unknown_7:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_9unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_sequential_8_layer_call_and_return_conditional_losses_561308p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:���������
!
_user_specified_name	input_9:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
J
.__inference_max_pooling1d_layer_call_fn_561795

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *=
_output_shapes+
):'���������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *R
fMRK
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_560979v
IdentityIdentityPartitionedCall:output:0*
T0*=
_output_shapes+
):'���������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�
-
__inference__destroyer_561906
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�

�
C__inference_dense_8_layer_call_and_return_conditional_losses_561112

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0j
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0w
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������W
SoftmaxSoftmaxBiasAdd:output:0*
T0*(
_output_shapes
:����������a
IdentityIdentitySoftmax:softmax:0^NoOp*
T0*(
_output_shapes
:����������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�q
�
H__inference_sequential_8_layer_call_and_return_conditional_losses_561421
input_9X
Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handleY
Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value	/
+text_vectorization_string_lookup_10_equal_y2
.text_vectorization_string_lookup_10_selectv2_t	#
embedding_561403:	�Nd$
conv1d_561406:d�
conv1d_561408:	�"
dense_8_561415:
��
dense_8_561417:	�
identity��conv1d/StatefulPartitionedCall�dense_8/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2_
text_vectorization/StringLowerStringLowerinput_9*'
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2#text_vectorization/Squeeze:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
)text_vectorization/string_lookup_10/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0+text_vectorization_string_lookup_10_equal_y*
T0*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/SelectV2SelectV2-text_vectorization/string_lookup_10/Equal:z:0.text_vectorization_string_lookup_10_selectv2_tPtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/IdentityIdentity5text_vectorization/string_lookup_10/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:05text_vectorization/string_lookup_10/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
!embedding/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0embedding_561403*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_561059�
conv1d/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0conv1d_561406conv1d_561408*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_conv1d_layer_call_and_return_conditional_losses_561079�
max_pooling1d/PartitionedCallPartitionedCall'conv1d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *R
fMRK
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_560979�
dropout_1/PartitionedCallPartitionedCall&max_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_561091�
$global_max_pooling1d/PartitionedCallPartitionedCall"dropout_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_560992�
dropout_2/PartitionedCallPartitionedCall-global_max_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_2_layer_call_and_return_conditional_losses_561099�
dense_8/StatefulPartitionedCallStatefulPartitionedCall"dropout_2/PartitionedCall:output:0dense_8_561415dense_8_561417*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_561112x
IdentityIdentity(dense_8/StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:�����������
NoOpNoOp^conv1d/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall"^embedding/StatefulPartitionedCallH^text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2@
conv1d/StatefulPartitionedCallconv1d/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2�
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:P L
'
_output_shapes
:���������
!
_user_specified_name	input_9:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�

�
-__inference_sequential_8_layer_call_fn_561140
input_9
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:
��
	unknown_7:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_9unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_sequential_8_layer_call_and_return_conditional_losses_561119p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:���������
!
_user_specified_name	input_9:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�	
d
E__inference_dropout_2_layer_call_and_return_conditional_losses_561170

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @e
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������p
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������j
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������Z
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�
H__inference_sequential_8_layer_call_and_return_conditional_losses_561724

inputsX
Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handleY
Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value	/
+text_vectorization_string_lookup_10_equal_y2
.text_vectorization_string_lookup_10_selectv2_t	4
!embedding_embedding_lookup_561677:	�NdI
2conv1d_conv1d_expanddims_1_readvariableop_resource:d�5
&conv1d_biasadd_readvariableop_resource:	�:
&dense_8_matmul_readvariableop_resource:
��6
'dense_8_biasadd_readvariableop_resource:	�
identity��conv1d/BiasAdd/ReadVariableOp�)conv1d/Conv1D/ExpandDims_1/ReadVariableOp�dense_8/BiasAdd/ReadVariableOp�dense_8/MatMul/ReadVariableOp�embedding/embedding_lookup�Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2^
text_vectorization/StringLowerStringLowerinputs*'
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2#text_vectorization/Squeeze:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Ttext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Utext_vectorization_string_lookup_10_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
)text_vectorization/string_lookup_10/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0+text_vectorization_string_lookup_10_equal_y*
T0*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/SelectV2SelectV2-text_vectorization/string_lookup_10/Equal:z:0.text_vectorization_string_lookup_10_selectv2_tPtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
,text_vectorization/string_lookup_10/IdentityIdentity5text_vectorization/string_lookup_10/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:05text_vectorization/string_lookup_10/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_561677?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tindices0	*4
_class*
(&loc:@embedding/embedding_lookup/561677*,
_output_shapes
:����������d*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/561677*,
_output_shapes
:����������d�
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dg
conv1d/Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
conv1d/Conv1D/ExpandDims
ExpandDims.embedding/embedding_lookup/Identity_1:output:0%conv1d/Conv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
)conv1d/Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp2conv1d_conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0`
conv1d/Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
conv1d/Conv1D/ExpandDims_1
ExpandDims1conv1d/Conv1D/ExpandDims_1/ReadVariableOp:value:0'conv1d/Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
conv1d/Conv1DConv2D!conv1d/Conv1D/ExpandDims:output:0#conv1d/Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
conv1d/Conv1D/SqueezeSqueezeconv1d/Conv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

����������
conv1d/BiasAdd/ReadVariableOpReadVariableOp&conv1d_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv1d/BiasAddBiasAddconv1d/Conv1D/Squeeze:output:0%conv1d/BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������d
conv1d/ReluReluconv1d/BiasAdd:output:0*
T0*-
_output_shapes
:�����������^
max_pooling1d/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :�
max_pooling1d/ExpandDims
ExpandDimsconv1d/Relu:activations:0%max_pooling1d/ExpandDims/dim:output:0*
T0*1
_output_shapes
:������������
max_pooling1d/MaxPoolMaxPool!max_pooling1d/ExpandDims:output:0*0
_output_shapes
:���������b�*
ksize
*
paddingVALID*
strides
�
max_pooling1d/SqueezeSqueezemax_pooling1d/MaxPool:output:0*
T0*,
_output_shapes
:���������b�*
squeeze_dims
\
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @�
dropout_1/dropout/MulMulmax_pooling1d/Squeeze:output:0 dropout_1/dropout/Const:output:0*
T0*,
_output_shapes
:���������b�e
dropout_1/dropout/ShapeShapemax_pooling1d/Squeeze:output:0*
T0*
_output_shapes
:�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*,
_output_shapes
:���������b�*
dtype0e
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:���������b��
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:���������b��
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*,
_output_shapes
:���������b�l
*global_max_pooling1d/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_max_pooling1d/MaxMaxdropout_1/dropout/Mul_1:z:03global_max_pooling1d/Max/reduction_indices:output:0*
T0*(
_output_shapes
:����������\
dropout_2/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @�
dropout_2/dropout/MulMul!global_max_pooling1d/Max:output:0 dropout_2/dropout/Const:output:0*
T0*(
_output_shapes
:����������h
dropout_2/dropout/ShapeShape!global_max_pooling1d/Max:output:0*
T0*
_output_shapes
:�
.dropout_2/dropout/random_uniform/RandomUniformRandomUniform dropout_2/dropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype0e
 dropout_2/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout_2/dropout/GreaterEqualGreaterEqual7dropout_2/dropout/random_uniform/RandomUniform:output:0)dropout_2/dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:�����������
dropout_2/dropout/CastCast"dropout_2/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:�����������
dropout_2/dropout/Mul_1Muldropout_2/dropout/Mul:z:0dropout_2/dropout/Cast:y:0*
T0*(
_output_shapes
:�����������
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_8/MatMulMatMuldropout_2/dropout/Mul_1:z:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:�����������
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������g
dense_8/SoftmaxSoftmaxdense_8/BiasAdd:output:0*
T0*(
_output_shapes
:����������i
IdentityIdentitydense_8/Softmax:softmax:0^NoOp*
T0*(
_output_shapes
:�����������
NoOpNoOp^conv1d/BiasAdd/ReadVariableOp*^conv1d/Conv1D/ExpandDims_1/ReadVariableOp^dense_8/BiasAdd/ReadVariableOp^dense_8/MatMul/ReadVariableOp^embedding/embedding_lookupH^text_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2>
conv1d/BiasAdd/ReadVariableOpconv1d/BiasAdd/ReadVariableOp2V
)conv1d/Conv1D/ExpandDims_1/ReadVariableOp)conv1d/Conv1D/ExpandDims_1/ReadVariableOp2@
dense_8/BiasAdd/ReadVariableOpdense_8/BiasAdd/ReadVariableOp2>
dense_8/MatMul/ReadVariableOpdense_8/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup2�
Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2Gtext_vectorization/string_lookup_10/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
E__inference_embedding_layer_call_and_return_conditional_losses_561059

inputs	*
embedding_lookup_561053:	�Nd
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_561053inputs*
Tindices0	**
_class 
loc:@embedding_lookup/561053*,
_output_shapes
:����������d*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0**
_class 
loc:@embedding_lookup/561053*,
_output_shapes
:����������d�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dx
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*,
_output_shapes
:����������dY
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 2$
embedding_lookupembedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
__inference_<lambda>_5619149
5key_value_init494894_lookuptableimportv2_table_handle1
-key_value_init494894_lookuptableimportv2_keys3
/key_value_init494894_lookuptableimportv2_values	
identity��(key_value_init494894/LookupTableImportV2�
(key_value_init494894/LookupTableImportV2LookupTableImportV25key_value_init494894_lookuptableimportv2_table_handle-key_value_init494894_lookuptableimportv2_keys/key_value_init494894_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: q
NoOpNoOp)^key_value_init494894/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
: :�N:�N2T
(key_value_init494894/LookupTableImportV2(key_value_init494894/LookupTableImportV2:!

_output_shapes	
:�N:!

_output_shapes	
:�N
�
e
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_561803

inputs
identityP
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :�

ExpandDims
ExpandDimsinputsExpandDims/dim:output:0*
T0*A
_output_shapes/
-:+����������������������������
MaxPoolMaxPoolExpandDims:output:0*A
_output_shapes/
-:+���������������������������*
ksize
*
paddingVALID*
strides
�
SqueezeSqueezeMaxPool:output:0*
T0*=
_output_shapes+
):'���������������������������*
squeeze_dims
n
IdentityIdentitySqueeze:output:0*
T0*=
_output_shapes+
):'���������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�
�
B__inference_conv1d_layer_call_and_return_conditional_losses_561790

inputsB
+conv1d_expanddims_1_readvariableop_resource:d�.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�"Conv1D/ExpandDims_1/ReadVariableOp`
Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
Conv1D/ExpandDims
ExpandDimsinputsConv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
"Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp+conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0Y
Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
Conv1D/ExpandDims_1
ExpandDims*Conv1D/ExpandDims_1/ReadVariableOp:value:0 Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
Conv1DConv2DConv1D/ExpandDims:output:0Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
Conv1D/SqueezeSqueezeConv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

���������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
BiasAddBiasAddConv1D/Squeeze:output:0BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������V
ReluReluBiasAdd:output:0*
T0*-
_output_shapes
:�����������g
IdentityIdentityRelu:activations:0^NoOp*
T0*-
_output_shapes
:������������
NoOpNoOp^BiasAdd/ReadVariableOp#^Conv1D/ExpandDims_1/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������d: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2H
"Conv1D/ExpandDims_1/ReadVariableOp"Conv1D/ExpandDims_1/ReadVariableOp:T P
,
_output_shapes
:����������d
 
_user_specified_nameinputs
�

�
C__inference_dense_8_layer_call_and_return_conditional_losses_561888

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0j
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0w
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������W
SoftmaxSoftmaxBiasAdd:output:0*
T0*(
_output_shapes
:����������a
IdentityIdentitySoftmax:softmax:0^NoOp*
T0*(
_output_shapes
:����������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

�
-__inference_sequential_8_layer_call_fn_561542

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:
��
	unknown_7:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_sequential_8_layer_call_and_return_conditional_losses_561308p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
c
*__inference_dropout_1_layer_call_fn_561813

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:���������b�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_561193t
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*,
_output_shapes
:���������b�`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:���������b�22
StatefulPartitionedCallStatefulPartitionedCall:T P
,
_output_shapes
:���������b�
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_2:0StatefulPartitionedCall_38"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
;
input_90
serving_default_input_9:0���������>
dense_83
StatefulPartitionedCall_1:0����������tensorflow/serving/predict:��
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer-3
layer-4
layer-5
layer-6
layer_with_weights-2
layer-7
		optimizer

	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature

signatures"
_tf_keras_sequential
;
_lookup_layer
	keras_api"
_tf_keras_layer
�

embeddings
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
	variables
trainable_variables
regularization_losses
 	keras_api
!__call__
*"&call_and_return_all_conditional_losses"
_tf_keras_layer
�
#	variables
$trainable_variables
%regularization_losses
&	keras_api
'__call__
*(&call_and_return_all_conditional_losses"
_tf_keras_layer
�
)	variables
*trainable_variables
+regularization_losses
,	keras_api
-_random_generator
.__call__
*/&call_and_return_all_conditional_losses"
_tf_keras_layer
�
0	variables
1trainable_variables
2regularization_losses
3	keras_api
4__call__
*5&call_and_return_all_conditional_losses"
_tf_keras_layer
�
6	variables
7trainable_variables
8regularization_losses
9	keras_api
:_random_generator
;__call__
*<&call_and_return_all_conditional_losses"
_tf_keras_layer
�

=kernel
>bias
?	variables
@trainable_variables
Aregularization_losses
B	keras_api
C__call__
*D&call_and_return_all_conditional_losses"
_tf_keras_layer
�
Eiter

Fbeta_1

Gbeta_2
	Hdecay
Ilearning_ratem�m�m�=m�>m�v�v�v�=v�>v�"
	optimizer
C
0
1
2
=3
>4"
trackable_list_wrapper
C
0
1
2
=3
>4"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Jnon_trainable_variables

Klayers
Lmetrics
Mlayer_regularization_losses
Nlayer_metrics

	variables
trainable_variables
regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�2�
-__inference_sequential_8_layer_call_fn_561140
-__inference_sequential_8_layer_call_fn_561519
-__inference_sequential_8_layer_call_fn_561542
-__inference_sequential_8_layer_call_fn_561352�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
H__inference_sequential_8_layer_call_and_return_conditional_losses_561626
H__inference_sequential_8_layer_call_and_return_conditional_losses_561724
H__inference_sequential_8_layer_call_and_return_conditional_losses_561421
H__inference_sequential_8_layer_call_and_return_conditional_losses_561490�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
!__inference__wrapped_model_560967input_9"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
,
Oserving_default"
signature_map
P
Pinput_vocabulary
Qlookup_table
R	keras_api"
_tf_keras_layer
"
_generic_user_object
':%	�Nd2embedding/embeddings
'
0"
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Snon_trainable_variables

Tlayers
Umetrics
Vlayer_regularization_losses
Wlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_embedding_layer_call_fn_561756�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_embedding_layer_call_and_return_conditional_losses_561765�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
$:"d�2conv1d/kernel
:�2conv1d/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Xnon_trainable_variables

Ylayers
Zmetrics
[layer_regularization_losses
\layer_metrics
	variables
trainable_variables
regularization_losses
!__call__
*"&call_and_return_all_conditional_losses
&""call_and_return_conditional_losses"
_generic_user_object
�2�
'__inference_conv1d_layer_call_fn_561774�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_conv1d_layer_call_and_return_conditional_losses_561790�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
]non_trainable_variables

^layers
_metrics
`layer_regularization_losses
alayer_metrics
#	variables
$trainable_variables
%regularization_losses
'__call__
*(&call_and_return_all_conditional_losses
&("call_and_return_conditional_losses"
_generic_user_object
�2�
.__inference_max_pooling1d_layer_call_fn_561795�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_561803�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
bnon_trainable_variables

clayers
dmetrics
elayer_regularization_losses
flayer_metrics
)	variables
*trainable_variables
+regularization_losses
.__call__
*/&call_and_return_all_conditional_losses
&/"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
�2�
*__inference_dropout_1_layer_call_fn_561808
*__inference_dropout_1_layer_call_fn_561813�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
E__inference_dropout_1_layer_call_and_return_conditional_losses_561818
E__inference_dropout_1_layer_call_and_return_conditional_losses_561830�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
gnon_trainable_variables

hlayers
imetrics
jlayer_regularization_losses
klayer_metrics
0	variables
1trainable_variables
2regularization_losses
4__call__
*5&call_and_return_all_conditional_losses
&5"call_and_return_conditional_losses"
_generic_user_object
�2�
5__inference_global_max_pooling1d_layer_call_fn_561835�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_561841�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
lnon_trainable_variables

mlayers
nmetrics
olayer_regularization_losses
player_metrics
6	variables
7trainable_variables
8regularization_losses
;__call__
*<&call_and_return_all_conditional_losses
&<"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
�2�
*__inference_dropout_2_layer_call_fn_561846
*__inference_dropout_2_layer_call_fn_561851�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
E__inference_dropout_2_layer_call_and_return_conditional_losses_561856
E__inference_dropout_2_layer_call_and_return_conditional_losses_561868�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
": 
��2dense_8/kernel
:�2dense_8/bias
.
=0
>1"
trackable_list_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
qnon_trainable_variables

rlayers
smetrics
tlayer_regularization_losses
ulayer_metrics
?	variables
@trainable_variables
Aregularization_losses
C__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses"
_generic_user_object
�2�
(__inference_dense_8_layer_call_fn_561877�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_dense_8_layer_call_and_return_conditional_losses_561888�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
X
0
1
2
3
4
5
6
7"
trackable_list_wrapper
.
v0
w1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
$__inference_signature_wrapper_561749input_9"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
j
x_initializer
y_create_resource
z_initialize
{_destroy_resourceR jCustom.StaticHashTable
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
N
	|total
	}count
~	variables
	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
"
_generic_user_object
�2�
__inference__creator_561893�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__initializer_561901�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_561906�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
:  (2total
:  (2count
.
|0
}1"
trackable_list_wrapper
-
~	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
,:*	�Nd2Adam/embedding/embeddings/m
):'d�2Adam/conv1d/kernel/m
:�2Adam/conv1d/bias/m
':%
��2Adam/dense_8/kernel/m
 :�2Adam/dense_8/bias/m
,:*	�Nd2Adam/embedding/embeddings/v
):'d�2Adam/conv1d/kernel/v
:�2Adam/conv1d/bias/v
':%
��2Adam/dense_8/kernel/v
 :�2Adam/dense_8/bias/v
	J
Const
J	
Const_1
J	
Const_2
J	
Const_3
J	
Const_47
__inference__creator_561893�

� 
� "� 9
__inference__destroyer_561906�

� 
� "� B
__inference__initializer_561901Q���

� 
� "� �
!__inference__wrapped_model_560967tQ���=>0�-
&�#
!�
input_9���������
� "2�/
-
dense_8"�
dense_8�����������
B__inference_conv1d_layer_call_and_return_conditional_losses_561790g4�1
*�'
%�"
inputs����������d
� "+�(
!�
0�����������
� �
'__inference_conv1d_layer_call_fn_561774Z4�1
*�'
%�"
inputs����������d
� "�������������
C__inference_dense_8_layer_call_and_return_conditional_losses_561888^=>0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� }
(__inference_dense_8_layer_call_fn_561877Q=>0�-
&�#
!�
inputs����������
� "������������
E__inference_dropout_1_layer_call_and_return_conditional_losses_561818f8�5
.�+
%�"
inputs���������b�
p 
� "*�'
 �
0���������b�
� �
E__inference_dropout_1_layer_call_and_return_conditional_losses_561830f8�5
.�+
%�"
inputs���������b�
p
� "*�'
 �
0���������b�
� �
*__inference_dropout_1_layer_call_fn_561808Y8�5
.�+
%�"
inputs���������b�
p 
� "����������b��
*__inference_dropout_1_layer_call_fn_561813Y8�5
.�+
%�"
inputs���������b�
p
� "����������b��
E__inference_dropout_2_layer_call_and_return_conditional_losses_561856^4�1
*�'
!�
inputs����������
p 
� "&�#
�
0����������
� �
E__inference_dropout_2_layer_call_and_return_conditional_losses_561868^4�1
*�'
!�
inputs����������
p
� "&�#
�
0����������
� 
*__inference_dropout_2_layer_call_fn_561846Q4�1
*�'
!�
inputs����������
p 
� "�����������
*__inference_dropout_2_layer_call_fn_561851Q4�1
*�'
!�
inputs����������
p
� "������������
E__inference_embedding_layer_call_and_return_conditional_losses_561765a0�-
&�#
!�
inputs����������	
� "*�'
 �
0����������d
� �
*__inference_embedding_layer_call_fn_561756T0�-
&�#
!�
inputs����������	
� "�����������d�
P__inference_global_max_pooling1d_layer_call_and_return_conditional_losses_561841wE�B
;�8
6�3
inputs'���������������������������
� ".�+
$�!
0������������������
� �
5__inference_global_max_pooling1d_layer_call_fn_561835jE�B
;�8
6�3
inputs'���������������������������
� "!��������������������
I__inference_max_pooling1d_layer_call_and_return_conditional_losses_561803�E�B
;�8
6�3
inputs'���������������������������
� ";�8
1�.
0'���������������������������
� �
.__inference_max_pooling1d_layer_call_fn_561795wE�B
;�8
6�3
inputs'���������������������������
� ".�+'����������������������������
H__inference_sequential_8_layer_call_and_return_conditional_losses_561421pQ���=>8�5
.�+
!�
input_9���������
p 

 
� "&�#
�
0����������
� �
H__inference_sequential_8_layer_call_and_return_conditional_losses_561490pQ���=>8�5
.�+
!�
input_9���������
p

 
� "&�#
�
0����������
� �
H__inference_sequential_8_layer_call_and_return_conditional_losses_561626oQ���=>7�4
-�*
 �
inputs���������
p 

 
� "&�#
�
0����������
� �
H__inference_sequential_8_layer_call_and_return_conditional_losses_561724oQ���=>7�4
-�*
 �
inputs���������
p

 
� "&�#
�
0����������
� �
-__inference_sequential_8_layer_call_fn_561140cQ���=>8�5
.�+
!�
input_9���������
p 

 
� "������������
-__inference_sequential_8_layer_call_fn_561352cQ���=>8�5
.�+
!�
input_9���������
p

 
� "������������
-__inference_sequential_8_layer_call_fn_561519bQ���=>7�4
-�*
 �
inputs���������
p 

 
� "������������
-__inference_sequential_8_layer_call_fn_561542bQ���=>7�4
-�*
 �
inputs���������
p

 
� "������������
$__inference_signature_wrapper_561749Q���=>;�8
� 
1�.
,
input_9!�
input_9���������"2�/
-
dense_8"�
dense_8����������